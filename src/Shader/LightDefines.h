// ライト用定数
// C++とHLSLの両方からインクルードされるヘッダ
//  主に定数などを記述
static const int MAX_DIRLIGHT	= 1;	// 平行光源最大数
static const int MAX_POINTLIGHT = 100;	// ポイントライト最大数
static const int MAX_SPOTLIGHT	= 20;	// スポットライト最大数
