
//	Texture
Texture2D		ColorTex	: register(t0);
SamplerState	ColorSmp	: register(s1);

//
cbuffer bGauss : register(b0)
{
    float4 Weights0;
	float4 Weights1;
    float2 Offset;
    float2 Size;
    float2 Texel;
}

cbuffer bView : register(b1)
{
    row_major float4x4 g2DView;
}

struct VsIn
{
    float4 pos	: POSITION;
    float2 uv	: TEXCOORD;
};
struct VsOut
{
    float4 pos	: SV_Position;
    float2 uv	: TEXCOORD0;
};

struct VsGaussOut
{
    float4 pos		 : SV_Position;
    float2 texcoord0 : TEXCOORD0;
    float2 texcoord1 : TEXCOORD1;
    float2 texcoord2 : TEXCOORD2;
    float2 texcoord3 : TEXCOORD3;
    float2 texcoord4 : TEXCOORD4;
    float2 texcoord5 : TEXCOORD5;
    float2 texcoord6 : TEXCOORD6;
    float2 texcoord7 : TEXCOORD7;
};
typedef VsGaussOut PsGaussIn;

struct PsOut
{
    float4 Col : SV_Target0;
};