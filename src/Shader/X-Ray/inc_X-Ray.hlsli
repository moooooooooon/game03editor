cbuffer cbXRay : register(b0)
{
    row_major float4x4	g2DView;
    float4				g_XColor;
   // int FresnelEnable;	//	フレネル反射を用いて輪郭強調を行う(キャラの法線が必要)
}


Texture2D CharaDepthTex			: register(t0);		//	キャラだけが書き込まれた深度テクスチャ
Texture2D AllDepthTex			: register(t1);		//	全ての深度値が書き込まれたテクスチャ
Texture2D BaseTex				: register(t2);		//	通常のバックバッファ画像
Texture2D RefTex				: register(t3);		//	通常のバックバッファ画像
SamplerState WrapSmp			: register(s0);		// Wrap用のサンプラ


//
struct VsIn
{
    float4 pos	: POSITION;
    float2 uv	: TEXCOORD;
};

struct VsOut
{
    float4 pos	: SV_Position;
    float2 uv	: TEXCOORD;
};
typedef VsOut PsIn;

struct PsOut
{
    float4 Col	: SV_Target0;
};