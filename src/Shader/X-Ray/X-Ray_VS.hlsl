#include "inc_X-Ray.hlsli"

VsOut main(VsIn In)
{
    VsOut Out;
	
    Out.pos = mul(In.pos, g2DView);
    Out.uv  = In.uv;

    return Out;
}