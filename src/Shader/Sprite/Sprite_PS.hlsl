#include "inc_Sprite.hlsli"

//============================================
// 2D描画 ピクセルシェーダ
//============================================
float4 main(VS_OUT In) : SV_Target0
{
	// テクスチャ色 * 指定色
	return InputTex.Sample(PointClampSmp, In.UV) * In.Color;
}
