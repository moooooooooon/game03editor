/*=====================================================================*/
//	標準頂点レイアウト
//	シャドウマップでも共同で使うのでここに
/*=====================================================================*/

//	標準レイアウト
struct VsStandardIn
{
    float4 pos		: POSITION;		// 頂点座標(ローカル)
    float3 tangent	: TANGENT;		// 接線
    float3 binormal : BINORMAL;		// 従法線
    float3 normal	: NORMAL;		// 法線
    float2 uv		: TEXCOORD0;	// UV
};

//	スキンメッシュ用
struct VsSkinningIn
{
    float4 pos		: POSITION;
    float3 tangent	: TANGENT;
    float3 binormal : BINORMAL;
    float3 normal	: NORMAL;
    float2 uv		: TEXCOORD0;
    float4 blendWeight	: BLENDWEIGHT;	// ボーンウェイトx4
    uint4 blendIndex	: BLENDINDICES; // ボーン番号x4
};

//	スタティックメッシュのインスタンシング用
struct VsStaticInstIn
{
    float4 pos		: POSITION;			// 頂点座標(ローカル)
    float3 tangent	: TANGENT;			// 接線
    float3 binormal : BINORMAL;			// 従法線
    float3 normal	: NORMAL;			// 法線
    float2 uv		: TEXCOORD0;		// UV
    row_major float4x4 instanceMat : MATRIX;	// ワールド行列
    uint instID		: SV_InstanceID;			// インスタンスID
};


// 頂点シェーダ出力データ
struct VsOut
{
	float4 Pos		: SV_Position;		// 2D座標(射影座標系)
	float2 UV		: TEXCOORD0;		// UV
	float3 wT		: TEXCOORD1;		// 接線(Tangent)
	float3 wB		: TEXCOORD2;		// 従法線(BiNormal)
	float3 wN		: TEXCOORD3;		// 法線(Normal)
	float3 wPos		: TEXCOORD4;		// 3D座標(ワールド座標系)
	float3 wvPos	: TEXCOORD5;		// 3D座標(ビュー座標系)
	float3 DefN		: TEXCOORD6;		// 加工なし法線 (追加)
	float2 Depth	: TEXCOORD7;		//	深度値

};
typedef VsOut PsIn;