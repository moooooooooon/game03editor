//======================================
// デバッグよう定数バッファ
//======================================
struct DebugData
{
    int showCascadeDist;
};

cbuffer cbDebug : register(b13)
{
    DebugData   g_Debug;
};