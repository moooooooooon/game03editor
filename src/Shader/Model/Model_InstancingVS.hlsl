#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"


//===============================================
// スタティックメッシュ用頂点シェーダー
//===============================================
VsOut main(VsStaticInstIn In)
{
    VsOut Out = (VsOut) 0;
	
//	float4x4 mW = In.instanceMat;

	//-------------------------------------
	// 座標変換
	//-------------------------------------
    Out.Pos		= mul(In.pos, In.instanceMat);	// ワールド変換
	Out.wPos	= Out.Pos.xyz;					// ワールド座標を憶えておく
	Out.Pos		= mul(Out.Pos, g_mV);			// ビュー変換
	Out.wvPos	= Out.Pos.xyz;					// ビュー座標を憶えておく
	Out.Pos		= mul(Out.Pos, g_mP);			// 射影変換

	//-------------------------------------
	// UVはそのまま入れる
	//-------------------------------------
	Out.UV		= In.uv;

	//-------------------------------------
	// ３種の法線
	//-------------------------------------
	Out.wB		= normalize(mul(In.binormal, (float3x3)In.instanceMat));
	Out.wT		= normalize(mul(In.tangent, (float3x3)In.instanceMat));
	Out.wN		= normalize(mul(In.normal, (float3x3)In.instanceMat));
	
	// 加工なしの法線
	Out.DefN	= In.normal;

	return Out;
}
