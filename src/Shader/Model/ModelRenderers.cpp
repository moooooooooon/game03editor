#include "MainFrame/ZMainFrame.h"
#include "Shader/ShaderManager.h"

bool ModelRenderers::Init()
{
	Release();

	bool successed;

	m_SMeshRenderer					= Make_Shared(StaticMeshRenderer,sysnew);
	m_SkinMeshRenderer				= Make_Shared(SkinMeshRenderer,sysnew);
	m_InstSMeshRenderer				= Make_Shared(InstancingStaticMeshRenderer,sysnew);
	m_DebugColliderMeshRenderer		= Make_Shared(DebugColliderMeshRenderer,sysnew);
	m_StaticModelRender				= Make_Shared(StaticModelRenderer,sysnew);
	m_CollisionRender				= Make_Shared(ColliderRenderer, sysnew);

	successed  = m_SMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_PS.cso");
	successed &= m_SkinMeshRenderer->Init("Shader/Model_SkinVS.cso", "Shader/Model_PS.cso");
	successed &= m_InstSMeshRenderer->Init("Shader/Model_InstancingVS.cso", "Shader/Model_PS.cso");
	successed &= m_DebugColliderMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_MonoPS.cso");


	//successed &= m_StaticModelRender->Init("Shader/Model_VS.cso", "Shader/Model_PS.cso");
	successed &= m_StaticModelRender->Init("Shader/Model_VS.cso", "Shader/Model_PS_Test_A.cso");


	successed &= m_CollisionRender->Init("Shader/Model_VS.cso", "Shader/CollisionCheck_PS.cso");

//	successed &= m_CollisionRender->Init("Shader/Model_VS.cso", "Model_MonoPS_B.cso");



	m_RenderTargets = Make_Shared(ZTextureSet,appnew,RenderTargetCnt);
	DXGI_FORMAT	fmt[] =
	{
		DXGI_FORMAT_R16G16B16A16_FLOAT,		//	色
		DXGI_FORMAT_R32_FLOAT,			//	カメラ
		//DXGI_FORMAT_R32G32_FLOAT,
		//DXGI_FORMAT_R16G16B16A16_FLOAT,		//	エッジかけるためにちょっと変更(カメラ)
		DXGI_FORMAT_R32_FLOAT,				//	キャラ
		DXGI_FORMAT_R8_UNORM,				//	X-Ray
	};
	m_RenderTargets->CreateRTSet(APP.m_Window->GetWidth(), APP.m_Window->GetHeight(), RenderTargetCnt, fmt);

	m_cb9_PerMaterial.Create(9);
	m_cb9_PerMaterial.SetPS();
	m_cb9_PerMaterial.SetVS();
	m_cb9_PerMaterial.SetGS();
	return successed;
}

void ModelRenderers::Release()
{
	if(m_SMeshRenderer)
		m_SMeshRenderer->Release();
	if(m_SkinMeshRenderer)
		m_SkinMeshRenderer->Release();
	if(m_InstSMeshRenderer)
		m_InstSMeshRenderer->Release();
	if (m_DebugColliderMeshRenderer)
		m_DebugColliderMeshRenderer->Release();


	m_StaticModelRender = nullptr;
	
	if (m_CollisionRender)
		m_CollisionRender->Release();

	m_cb9_PerMaterial.Release();

	if(m_RenderTargets)
		m_RenderTargets->Release();
}

void ModelRenderers::Begin3DRendering() {

	m_BackUp.GetNowAll();

	m_RenderTargets->AllClearRT(ZVec4(0, 0, 0, 0));

	m_RenderTargets->GetTex(1)->ClearRT(ZVec4(1, 1, 1, 1));

	m_RT.GetNowTop();

	for (int cnt = 0; cnt < m_RenderTargets->GetListSize(); cnt++)
		m_RT.RT(cnt, m_RenderTargets->GetTex(cnt)->GetRTTex());

	m_RT.SetToDevice();
}

void ModelRenderers::End3DRendering()
{
	m_BackUp.SetToDevice();
}

void ModelRenderers::Draw()
{
	ShMgr.m_LightScene->SetTexturePS(19);

	if (m_ZPreFlg)
	{
		m_SkinMeshRenderer->Flash();
		Z_Prepass();
	}
	else
	{
		m_SkinMeshRenderer->Flash();
	}

	//
	m_StaticModelRender->OutLine();

	//
	m_StaticModelRender->Flash();
	m_InstSMeshRenderer->Flash();

	ZDx.GetWhiteTex()->SetTexturePS(19);
}

void ModelRenderers::DrawCollider() {

	m_CollisionRender->SetMulColor(&ZVec4(0.f, 0.9f, 1.4f, 0.9f));
	m_CollisionRender->Flash();
	m_CollisionRender->SetMulColor(&ZVec4(1.f));
	
}

void ModelRenderers::DrawShadow()
{
	ZRenderTarget_BackUpper b;

	ShMgr.m_LightScene->ClearRT(ZVec4(1, 1, 1, 1));
	ShMgr.m_LightDepth->ClearDepth();


	ZRenderTargets rt;
	rt.RT(0, ShMgr.m_LightScene->GetRTTex());
	rt.Depth(ShMgr.m_LightDepth->GetDepthTex());
	
	ZDx.SetViewport(ShMgr.m_LightDepthTexSize.x, ShMgr.m_LightDepthTexSize.y);	
	rt.SetToDevice();

	m_StaticModelRender->Shadow();
	m_SkinMeshRenderer->Shadow();

}

void ModelRenderers::Z_Prepass() 
{
	m_StaticModelRender->Z_Prepass();
}

void ModelRenderers::SetConstBuffers()
{

	m_cb9_PerMaterial.WriteData();

	//...

}

void ModelRenderers::ImGui()
{
	static const string title[] = {
		"BasePass",
		"CameraDepth",
		"CharaDepth",
		"RefPow"
	};

	auto Targets = [this]
	{
		std::string imGuiWndName = "RenderTextures";
		if (ImGui::Begin(imGuiWndName.c_str()) == false) {
			ImGui::End();
			return;
		}

		ImGui::Checkbox("Use::Z-PrePass", &m_ZPreFlg);

		ImGui::BeginDockspace();
		if (ImGui::BeginDock("LightDepth")) {
			if (ShMgr.m_LightScene->GetTex()) {
				ImGui::Image(ShMgr.m_LightScene->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::EndDock();
		}


		for (int i = 0; i < m_RenderTargets->GetListSize(); i++) {
			if (ImGui::BeginDock(title[i].c_str())) {
				if (m_RenderTargets->GetTex(i)->GetTex()) {
					ImGui::Image(m_RenderTargets->GetTex(i)->GetTex(), ImGui::GetWindowSize());
				}
				ImGui::EndDock();
			}
		}

		ImGui::EndDockspace();



		ImGui::End();
	};

	DW_IMGUI_FUNC(Targets);


}