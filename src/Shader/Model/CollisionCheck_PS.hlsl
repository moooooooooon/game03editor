#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"
#include "../inc_LightCB.hlsli"
#include "../inc_CameraCB.hlsli"

//当たり判定チェックよう

// g_Diffuseを単色で出力するピクセルシェーダ
PsOut main(PsIn In)
{
	
	// このピクセルからカメラへの方向
    float3 vCam = normalize(g_CamPos - In.wPos);
    float3 w_normal = normalize(In.wN);

    float4 MateCol = 1;
	
    MateCol = g_MulColor;

	//	キャラ屈折
    float A = 0.6f; // 屈折率
    float B = dot(vCam, w_normal);
    float C = sqrt(1.0 - A * A * (1 - B * B));
    float Rs = (A * B - C) * (A * B - C) / ((A * B + C) * (A * B + C));
    float Rp = (A * C - B) * (A * C - B) / ((A * C + B) * (A * C + B));
    float power = (Rs + Rp) / 2.0f; 
    power = min(power + 0.20, 1.0); 



    PsOut Out = (PsOut) 0;
    Out.Color = MateCol;
    Out.Color.rgb -= power;	//	せっかくなんで色つけてみた
    return Out;
}