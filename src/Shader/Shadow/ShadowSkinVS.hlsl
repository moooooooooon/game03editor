#include "inc_Shadow.hlsli"
#include "../inc_LightCB.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"

//===============================================
// スキンメッシュ用頂点シェーダー
//===============================================
VsDepthOut main(VsDepthSkinIn In)
{
	row_major float4x4 mBones = 0; // 最終的なワールド行列
	[unroll]
    for (int i = 0; i < 4; i++)
    {
        mBones += g_mBones[In.blendIndex[i]] * In.blendWeight[i];
    }

    In.pos = mul(In.pos, mBones);
	
    VsDepthOut Out = (VsDepthOut) 0;
    Out.pos		 = mul(In.pos, g_mW);
    Out.pos		 = mul(Out.pos, LightVP);
    Out.Depth.xy = Out.pos.zw;

    Out.uv = In.uv;

	
	return Out;
}
