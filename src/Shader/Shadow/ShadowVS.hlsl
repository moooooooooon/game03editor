#include "inc_Shadow.hlsli"
#include "../inc_LightCB.hlsli"

VsDepthOut main(VsDepthIn In)
{

    VsDepthOut Out = (VsDepthOut) 0;
	
    Out.pos = mul(In.pos, g_mW);
    Out.pos = mul(Out.pos, LightVP);
	
    Out.Depth.xy = Out.pos.zw;

    Out.uv = In.uv;

    return Out;
}