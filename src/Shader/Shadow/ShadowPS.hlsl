#include "inc_Shadow.hlsli"

DepthOut main(PsDepthIn In)
{
    clip(MeshTex.Sample(WrapSmp, In.uv).a - 0.01);
	
    DepthOut Out = (DepthOut) 0;
    Out.Depth.rgb =In.Depth.x / In.Depth.y;
    Out.Depth.a = 1.f;
	return Out;
}