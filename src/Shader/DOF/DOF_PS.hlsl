#include "inc_DOF.hlsli"

PsOut main(PsIn In)
{
    float4 colA = BaseTex.Sample(Smp, In.uv);
    float4 colB = BlurTex.Sample(Smp, In.uv);
    float dofVal = DOFTex.Sample(Smp, In.uv).r;
 
    PsOut Out;
    Out.Col.rgba = colA.rgba * dofVal;
	Out.Col.rgba+= colB.rgba * (1.f - dofVal);
    return Out;
}