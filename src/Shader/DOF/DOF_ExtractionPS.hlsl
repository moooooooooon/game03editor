
#include "inc_DOF.hlsli"
#include "../inc_CameraCB.hlsli"

float4 main(PsIn In) : SV_TARGET
{
    float depth = BaseTex.Sample(Smp, In.uv).r;
    int g = 0;
    if (g_DoF_FarEnable)
    {
    
        if (depth > g_DoF_Far.x)
        {
            depth = g_DoF_Far.y * (1.0 - depth);
            g = 1;
        }
        else if (depth > g_DoF_Near.x)
        {
            depth = 1.0;
        }
        else
        {
            depth *= g_DoF_Near.y;
        }
    }
    else
    {
        if (depth > g_DoF_Near.x)
        {
            depth = 1.0;
        }
        else
        {
            depth *= g_DoF_Near.y;
        }

    }
	return float4(depth, g, 0.f, 1.0f);
}