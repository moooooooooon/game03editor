#include "MainFrame/ZMainFrame.h"
#include "../CommonECSComponents/CommonComponents.h"
#include "../TestECSConponents/TestComponents.h"
#include "TestSystems.h"

MoveUpdateSystem::MoveUpdateSystem()
{
	Init();
	m_UseMultiThread = true;
}

void MoveUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* motion = GetCompFromUpdateParam(MotionComponent,components);
	
	if ((trans->Transform.GetPos() - motion->StartPos).Length() >= 150)
	{
		trans->m_Entity->Remove();
		return;
	}

	ZVec3 newPos = trans->Transform.GetPos();
	ZVec3 newVel = motion->Velocity;
	ZVec3 acc = motion->Acceleration;
	forestRuth(newPos, newVel, acc, delta);
	trans->Transform.SetPos(newPos);
	motion->Velocity = newVel;

}

void MoveUpdateSystem::verlet(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta)
{
	float halfDelta = delta * 0.5f;
	pos += velocity * halfDelta;
	velocity += acceleration * delta;
	pos += velocity * halfDelta;
}

void MoveUpdateSystem::forestRuth(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta)
{
	static const float frCoefficient = 1.0f / (2.0f - pow(2.0f, 1.0f / 3.0f));
	static const float frComplement = 1.0f - 2.0f*frCoefficient;
	verlet(pos, velocity, acceleration, delta*frCoefficient);
	verlet(pos, velocity, acceleration, delta*frComplement);
	verlet(pos, velocity, acceleration, delta*frCoefficient);
}

AnimationUpdateSystem::AnimationUpdateSystem()
{
	Init();
	m_DebugSystemName = "AnimationUpdateSystem";
}

void AnimationUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* bc = GetCompFromUpdateParam(ModelBoneControllerConponent,components);
	auto* animator = GetCompFromUpdateParam(AnimatorComponent,components);
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);

	float fps = (1.0f / delta);
	animator->Animator->Animation(60.0f / fps);
	bc->BoneController->CalcBoneMatrix(false);
}

void AnimationUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto* bc = GetCompFromUpdateParam(ModelBoneControllerConponent,components);
	bc->BoneController->CalcBoneMatrix(true);
	bc->BoneController->UpdateBoneConstantBuffer();
}

void AnimationUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

StaticMeshDrawSystem::StaticMeshDrawSystem()
{
	Init();
	m_IsInstancingDraw = false;
	m_DebugSystemName = "StaticMeshDrawSyetem";
}

void StaticMeshDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* model = GetCompFromUpdateParam(GameModelComponent,components);

	if(m_IsInstancingDraw)
		ShMgr.m_Ms.m_InstSMeshRenderer->Submit(&trans->Transform, model->Model.GetPtr());
	else
		ShMgr.m_Ms.m_SMeshRenderer->Submit(&trans->Transform, model->Model.GetPtr());
}

void StaticMeshDrawSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
	ImGui::Checkbox("Use Instancing Renderer", &m_IsInstancingDraw);
}

SkinMeshDrawSystem::SkinMeshDrawSystem()
{
	Init();
	m_DebugSystemName = "SkinMeshDrawSystem";
}

void SkinMeshDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* model = GetCompFromUpdateParam(GameModelComponent,components);
	auto* boneController = GetCompFromUpdateParam(ModelBoneControllerConponent,components);

	auto& pos = trans->Transform.GetPos();
	ShMgr.m_Ms.m_SkinMeshRenderer->Submit(
		&trans->Transform,
		model->Model.GetPtr(),
		boneController->BoneController.GetPtr(),
		model->RenderFlg.GetPtr());
}

void SkinMeshDrawSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

BoxSpawnerSystem::BoxSpawnerSystem(ZAVector<ZSP<ECSEntity>>& entityList) : m_pEntityList(&entityList)
{
	m_BoxModel = APP.m_ResStg.LoadMesh("data/Model/box/box.xed");
	m_NumSpawnEnitites = 500;
	m_DebugSystemName = "BoxSpawnerSystem";
}

void BoxSpawnerSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	if (ECS.GetNumEntities() <= 5)
	{
		if (m_pEntityList == nullptr)
			return;
		//ECSEntity::RemoveAllEntity(*m_pEntityList);
		for (UINT i = 0; i < m_NumSpawnEnitites; i++)
		{
			TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
			MotionComponent* motioncomp = ECS.MakeComponent<MotionComponent>();
			GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();

			const ZVec3 pos(APP.m_Rand.GetFloat() * 10 - 5.0f, APP.m_Rand.GetFloat() * 10 - 5.0f, APP.m_Rand.GetFloat() * 10 - 5.0f + 20);
			transcomp->Transform.SetPos(pos);

			const float vf = -3.0f;
			const float af = 20.0f;
			motioncomp->StartPos = pos;
			motioncomp->Acceleration = ZVec3(APP.m_Rand.GetFloat(-af, af), APP.m_Rand.GetFloat(-af, af), APP.m_Rand.GetFloat(-af, af));
			motioncomp->Velocity = motioncomp->Acceleration*vf;
			modelcomp->Model = m_BoxModel;

			auto entity = ECS.MakeEntity(transcomp, motioncomp, modelcomp);
			(*m_pEntityList).push_back(std::move(entity));
		}
	}

}

void BoxSpawnerSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();

	const int min = 5;
#ifdef _DEBUG
	const int max = 2000;
#elif NDEBUG
	const int max = 50000;
#endif

	ImGui::SliderInt("Num Spawn Box", (int*)&m_NumSpawnEnitites, min, max);
}

void PhysicsSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	m_pPhysicsWorld->StepSimulation(delta);
}

CharaDebugSystem::CharaDebugSystem()
{
	Init();
	m_DebugSystemName = "CharaDebugSystem";
}

void CharaDebugSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto charaComp = GetCompFromUpdateParam(CharaComponent,components);

	if (transComp == nullptr || charaComp == nullptr)
		return;

	auto imGuiFunc = [this, transComp, charaComp]
	{
		if (ImGui::Begin(m_DebugSystemName.c_str()) == false)
		{
			ImGui::End();
			return;
		}
		
		ImGui::PushID(charaComp->Name.c_str());
		ImGui::Text("Name : %s", charaComp->Name.c_str());

		auto& mat = transComp->Transform;
		ZVec3 pos = mat.GetPos();
		ZVec3 scale = mat.GetScale();
		const float minScale = 0.01f;
		const float maxScale = 2.0f;

		ImGui::DragFloat3("pos", &pos[0],0.01f);
		ImGui::DragFloat("scale", &scale[0], 0.01f, 0.01f, 2.0f);
		btClamp(scale[0], minScale, maxScale);

		mat.SetScale(scale[0]);
		mat.SetPos(pos);
		
		ImGui::Separator();
		ImGui::PopID();

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);
}

void CharaDebugSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

VRControllSystem::VRControllSystem()
{
	Init();
}

void VRControllSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* vrcon = GetCompFromUpdateParam(VRControllerComponent,components);
	trans->Transform = vrcon->MScale * vrcon->m_VRCon.GetMatrix();
	vrcon->m_VRCon.Update();
}

AddColliderSystem::AddColliderSystem() {
	Init();
}

void AddColliderSystem::UpdateComponents(float delta, UpdateCompParams components) {
	auto modelComp = GetCompFromUpdateParam(GameModelComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// 当たり判定オブジェクトを登録する
	if (!modelComp->Model) {
		return;
	}

	if (modelComp->HitObj->m_MeshTbl.empty()) {
		modelComp->HitObj->ClearMesh();
		modelComp->HitObj->AddMesh(modelComp->Model);
	}
	modelComp->HitObj->Set(transComp->Transform);
	// m_ColEng に、このあたり判定を登録
	APP.m_ColEng.AddDef(modelComp->HitObj.GetPtr());

}















/*--------------------------------------------------------------------------------------------------------------------------------------*/

TestDrawSystem::TestDrawSystem() 
{
	Init();
	m_IsInstancingDraw = false;
	m_DebugSystemName = "StaticMeshDrawSyetem";
}

void TestDrawSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto* trans = GetCompFromUpdateParam(TransformComponent,components);
	auto* model = GetCompFromUpdateParam(GameModelComponent,components);

	//if (m_IsInstancingDraw)
		Renderer.m_StaticModelRender->Submit(&trans->Transform, model->Model.GetPtr(),model->RenderFlg.GetPtr());
	//else
	//	ShMgr.m_Ms.m_SMeshRenderer->Submit(&trans->Transform, model->Model.get());
}

void TestDrawSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
	ImGui::Checkbox("Use Instancing Renderer", &m_IsInstancingDraw);
}



ColliderDrawSystem::ColliderDrawSystem() {
	Init();
	m_DebugSystemName = "ColliderDrawSystem";
}

void ColliderDrawSystem::UpdateComponents(float delta, UpdateCompParams components) {
	auto* trans = GetCompFromUpdateParam(TransformComponent, components);
	auto* col = GetCompFromUpdateParam(ColliderComponent, components);

	if (col->HitModel.GetPtr()) {
		Renderer.m_CollisionRender->Submit(&trans->Transform, col->HitModel.GetPtr());
	}
}



LineDrawSystem::LineDrawSystem() {
	Init();
	m_DebugSystemName = "LineDrawSystem";
}

void LineDrawSystem::UpdateComponents(float delta, UpdateCompParams components) {
	auto* trans = GetCompFromUpdateParam(TransformComponent, components);
	auto* col = GetCompFromUpdateParam(ColliderComponent, components);

// Sphere
	if (col->HitSphere)
	{
		col->HitSphere->DebugDraw(1.f);
	}

// Box
	if (col->HitBox) {
		col->HitBox->DebugDraw(1.f);
	}

}










//SkinMeshDrawSystem::SkinMeshDrawSystem() {
//	AddComponentType<TransformComponent>();
//	AddComponentType<GameModelComponent>();
//	AddComponentType<ModelBoneControllerConponent>();
//	m_DebugSystemName = "SkinMeshDrawSystem";
//}
//
//void SkinMeshDrawSystem::UpdateComponents(float delta, UpdateCompParams components) {
//	auto* trans = GetCompFromUpdateParam<TransformComponent>(components);
//	auto* model = GetCompFromUpdateParam<GameModelComponent>(components);
//	auto* boneController = GetCompFromUpdateParam<ModelBoneControllerConponent>(components);
//
//	auto& pos = trans->Transform.GetPos();
//	ShMgr.m_Ms.m_SkinMeshRenderer->Submit(&trans->Transform, model->Model.get(), boneController->BoneController.get());
//}
//
//void SkinMeshDrawSystem::DebugImGuiRender() {
//	ECSSystemBase::DebugImGuiRender();
//}
