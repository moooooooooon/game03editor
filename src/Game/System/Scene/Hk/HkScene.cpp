#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSConponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"
#include "HkScene.h"

void HkScene::Release()
{
	m_DirLight = nullptr;
	m_PostEffect.Release();

	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	m_PhysicsWorld->Release();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();


}

void HkScene::Init()
{


	DW_SCROLL(0, "タイトルシーン初期化");

	m_PhysicsWorld = Make_Shared(ZPhysicsWorld,sysnew);

	m_PhysicsWorld->Init();

	// 平行サイト作成
	m_DirLight = ShMgr.m_LightMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(0.8f, -1, 0.8f),		// ライト方向
		ZVec4(0.7f, 0.7f, 0.7f, 1)	// 基本色
	);

	// 環境色
	ShMgr.m_LightMgr.m_AmbientLight.Set(0.3f, 0.3f, 0.3f);

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);

	// MMDモデル描画(PMXテスト PMDは後回し)
	{
			/*GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
			CharaComponent* characomp = ECS.MakeComponent<CharaComponent>();
			characomp->Name = "Alicia_solid";

			auto model = APP.m_ResStg.LoadMesh("data/Model/Alicia/MMD/Alicia_solid.pmx");
			modelcomp->Model = model;
			modelcomp->RenderFlg = make_shared<Object3D_RenderFlgs>();
			modelcomp->RenderFlg->Character = true;

			TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
			transcomp->Transform.CreateScale(0.1f, 0.1f, 0.1f);
			ZMatrix m;
			m.CreateMove(5, 0, 0);
			transcomp->Transform = transcomp->Transform * m;

			ModelBoneControllerConponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerConponent>();
			AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
			bonecontrollercomp->BoneController = std::make_shared<ZBoneController>();
			animatorcomp->Animator = std::make_shared<ZAnimator>();

			bonecontrollercomp->BoneController->SetModel(modelcomp->Model);
			bonecontrollercomp->BoneController->AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.get());

			sptr<ZAnimationSet> animeSet = std::make_shared<ZAnimationSet>();

			animeSet->LoadVMD("data/Model/Alicia/MMD Motion/2分ループステップ1.vmd", *bonecontrollercomp->BoneController, "モーション0");
			modelcomp->Model->GetAnimeList().push_back(animeSet);

			bonecontrollercomp->BoneController->InitAnimator(*animatorcomp->Animator);

			animatorcomp->Animator->ChangeAnime("モーション0", true);
			animatorcomp->Animator->EnableRootMotion(true);

			auto entity = ECS.MakeEntity(transcomp, modelcomp, bonecontrollercomp, animatorcomp, characomp);
			m_Entities.push_back(entity);*/
	}

	//	ステージ
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		//auto model = APP.m_ResStg.LoadMesh("data/TestRoom/Test.xed");
		auto model = APP.m_ResStg.LoadMesh("data/Map/map3/map.xed");
		modelcomp->Model				= model;
		modelcomp->RenderFlg			= Make_Shared(RenderState,appnew);
		modelcomp->RenderFlg->Character = false;

		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
		auto entity = ECS.MakeEntity(transcomp, modelcomp);
		m_Entities.push_back(entity);
	}

	//	テストキャラ
	{

		CharaComponent* characomp = ECS.MakeComponent<CharaComponent>();
		characomp->Name = "Chara";
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		auto model = APP.m_ResStg.LoadMesh("data/Model/Survivor/Survivor.xed");
		modelcomp->Model				= model;
		modelcomp->RenderFlg			= Make_Shared(RenderState,appnew);
		modelcomp->RenderFlg->Character = true;


		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

		ModelBoneControllerConponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerConponent>();
		AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
		bonecontrollercomp->BoneController = Make_Shared(ZBoneController,appnew);
		animatorcomp->Animator = Make_Shared(ZAnimator,appnew);

		bonecontrollercomp->BoneController->SetModel(modelcomp->Model);
		bonecontrollercomp->BoneController->AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.GetPtr());
		
		bonecontrollercomp->BoneController->InitAnimator(*animatorcomp->Animator);
		animatorcomp->Animator->ChangeAnime("Movement", true);
		animatorcomp->Animator->EnableRootMotion(false);

		auto entity = ECS.MakeEntity(transcomp,modelcomp,bonecontrollercomp,animatorcomp,characomp);
		m_Entities.push_back(entity);
	}
	/*-------------------------------------------------------------------------------*/

	//	ポストエフェクトを作成
	m_PostEffect.Init();
	m_PostEffect.LoadState("data/Scene/HkScene/PostState.json");	//	ポストエフェクトステート入力
	//m_PostEffect.SaveState("data/Scene/HkScene/PostState.json");	//	ポストエフェクトステート出力
	
	//
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");

	ShMgr.m_Blur.CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));


	/*-------------------------------------------------------------------------------*/



	 //システム準備
	m_UpdateSystems.AddSystem(Make_Shared(MoveUpdateSystem,sysnew));
	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,sysnew));
//	m_UpdateSystems.AddSystem(std::make_shared<PhysicsSystem>(m_PhysicsWorld));
//	m_UpdateSystems.AddSystem(std::make_shared<CharaDebugSystem>());
//	m_DrawSystems.AddSystem(std::make_shared<StaticMeshDrawSystem>());
	m_DrawSystems.AddSystem(Make_Shared(TestDrawSystem,sysnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,sysnew));




}

void HkScene::Update()
{
	DW_STATIC(1, "Hk_Scene");

	//// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
		APP.ExitGameLoop();
		return;
	}

	//カメラ操作
	m_Cam.Update();

	ECS.UpdateSystems(m_UpdateSystems, APP.m_DeltaTime);

	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());

}

void HkScene::ImGuiUpdate()
{

	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = m_PhysicsWorld->IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			m_PhysicsWorld->SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);

	Renderer.ImGui();
	m_PostEffect.ImGui();
	ShMgr.m_Blur.ImGui();
	ShMgr.m_LightMgr.ImGui();

}

void HkScene::Draw()
{
	//	3D描画開始
	Renderer.Begin3DRendering();{


		// 半透明モード
		ShMgr.m_bsAlpha.SetState();

		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();

		// カメラやライトのデータをシェーダ側に転送する
		{
			// カメラ設定& シェーダに書き込み
			m_Cam.SetCamera();
			// ライト情報をシェーダ側に書き込む
			ShMgr.m_LightMgr.Update();
			//	モデルシェーダの固定定数を書き込み
			ShMgr.m_Ms.SetConstBuffers();
		}

		// [2D]背景描画
		{
			ShMgr.m_Ss.Begin(false, true);
			ShMgr.m_Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, 1280, 720);
			ShMgr.m_Ss.End();
		}

		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

		// 物理エンジンのデバッグ描画
		m_PhysicsWorld->DebugDraw();


		//	シャドウマップ描画	
		ShMgr.SetShadowCamTargetPoint(m_Cam.mCam.GetPos());	//	現在のカメラをセット
		Renderer.DrawShadow();

		//	3D描画
		ShMgr.m_Ls.Flash();
		ShMgr.m_Ms.Draw();



		//	3Dエフェクト描画

		// ~~~~~~
	}	//	3D描画終了
	Renderer.End3DRendering();

	//	ポストエフェクト
	m_PostEffect.Execute();

	//	2D描画(UIなど)







}

