#pragma once

#include "Game/Camera/GameCamera.h"

class HkScene : public SceneBase
{
public:
	virtual ~HkScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();




public:
	// その他

	// エンティティハンドル
	ZAVector<ZSP<ECSEntity>> m_Entities; 

	// 平行光源
	ZSP<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;

	// テクスチャ
	ZSP<ZTexture> m_texBack;

	ZSP<ZPhysicsWorld> m_PhysicsWorld;


	//	ポストエフェクト用(宣言はどこかシングルトンクラス内に作った方がいいけど、テスト的にここで)
	PostEffect				m_PostEffect;



	//---------------------------------------------------------------------------
	ZSP<ZTexture> m_SkyTex;	//	テスト

};
