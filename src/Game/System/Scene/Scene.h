#ifndef __SCENE_H__
#define __SCENE_H__

// シーン基本クラス
class SceneBase
{
public:
	SceneBase()
	{
	}
	virtual ~SceneBase()
	{
	}

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void ImGuiUpdate() = 0;
	virtual void Draw() = 0;

public:
	// 更新システム
	ECSSystemList m_UpdateSystems;
	// 描画システム
	ECSSystemList m_DrawSystems;

};

// シーンファクトリ
class SceneManager
{
public:
	// 初期化
	void Init();

	// 更新
	void Update();

	// ImGui更新
	void ImGuiUpdate();

	// 描画
	void Draw();

	// 解放
	void Release()
	{
		m_NowScene = nullptr;
		m_NextChangeSceneName = "";
		m_SceneGenerateMap.clear();
	}

	// シーンを変更
	//  sceneName	... あらかじめInit()で登録しておいたシーン名
	//  ※即座には変更されず次のUpdateの前に変更が実行される
	void ChangeScene(const ZString& sceneName)
	{
		m_NextChangeSceneName = sceneName;
	}

	// 現在のシーンを取得
	ZSP<SceneBase> GetNowScene()
	{
		return m_NowScene;
	}

private:
	// 現在のシーン
	ZSP<SceneBase> m_NowScene;

	// シーン生成マップ
	ZSMap<ZString, std::function<ZSP<SceneBase>()>>	m_SceneGenerateMap;

	ZString m_NextChangeSceneName;

};


#endif