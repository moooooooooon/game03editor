#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSConponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"
#include "TestScene.h"

void TestScene::Release()
{
	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	m_PhysicsWorld->Release();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
}

void TestScene::Init()
{
	DW_SCROLL(0, "タイトルシーン初期化");

	m_PhysicsWorld = Make_Shared(ZPhysicsWorld,sysnew);

	m_PhysicsWorld->Init();

	// 平行サイト作成
	m_DirLight = ShMgr.m_LightMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(1, -1, -0.5f),		// ライト方向
		ZVec4(0.8f, 0.8f, 0.8f, 1)	// 基本色
	);

	// 環境色
	ShMgr.m_LightMgr.m_AmbientLight.Set(0.2f, 0.2f, 0.2f);

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);

	/*
	// MMDモデル描画(PMXテスト PMDは後回し)
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		CharaComponent* characomp = ECS.MakeComponent<CharaComponent>();
		characomp->Name = "Alicia_solid";

		auto model = APP.m_ResStg.LoadMesh("data/Model/Alicia/MMD/Alicia_solid.pmx");
		modelcomp->Model = model;

		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();	
		
		ModelBoneControllerConponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerConponent>();
		AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
		bonecontrollercomp->BoneController = std::make_shared<ZBoneController>();
		animatorcomp->Animator = std::make_shared<ZAnimator>();
		
		bonecontrollercomp->BoneController->SetModel(modelcomp->Model);
		bonecontrollercomp->BoneController->AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.get());

		ZSP<ZAnimationSet> animeSet = std::make_shared<ZAnimationSet>();
		
		animeSet->LoadVMD("data/Model/Alicia/MMD Motion/2分ループステップ1.vmd", *bonecontrollercomp->BoneController, "モーション0");
		modelcomp->Model->GetAnimeList().push_back(animeSet);

		bonecontrollercomp->BoneController->InitAnimator(*animatorcomp->Animator);

		animatorcomp->Animator->ChangeAnime("モーション0", true);
		animatorcomp->Animator->EnableRootMotion(true);

		auto entity = ECS.MakeEntity(transcomp,modelcomp,bonecontrollercomp,animatorcomp,characomp);

		m_Entities.push_back(entity);
	}
	*/

	// システム準備
	m_UpdateSystems.AddSystem(Make_Shared(MoveUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(BoxSpawnerSystem,appnew,m_Entities));
	//m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,appnew));
	//m_UpdateSystems.AddSystem(Make_Shared(PhysicsSystem,appnew,m_PhysicsWorld));
	//m_UpdateSystems.AddSystem(Make_Shared(CharaDebugSystem,appnew));
	m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem,appnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,appnew));

	
}

void TestScene::Update()
{
	DW_STATIC(1, "PerformanceTest_Scene");

	// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
	//	APP.m_SceneMgr.ChangeScene("VR");
		APP.ExitGameLoop();
		return;
	}

	//カメラ操作
	m_Cam.Update();
	
	ECS.UpdateSystems(m_UpdateSystems,APP.m_DeltaTime);
	
	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());
}

void TestScene::ImGuiUpdate()
{
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = m_PhysicsWorld->IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			m_PhysicsWorld->SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);
}

void TestScene::Draw()
{
	// 半透明モード
	ShMgr.m_bsAlpha.SetState();

	// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
	ShMgr.m_bsAlpha_AtoC.SetState();


	// 描画直前にシェーダのフレーム単位データを更新する
	// 主にカメラやライトのデータをシェーダ側に転送する
	{
		// カメラ設定& シェーダに書き込み
		m_Cam.SetCamera();
		// ライト情報をシェーダ側に書き込む
		ShMgr.m_LightMgr.Update();
	}

	// [2D]背景描画

	// ~~~~~~

	// [3D]モデル描画
	ECS.UpdateSystems(m_DrawSystems,APP.m_DeltaTime,true);

	// 物理エンジンのデバッグ描画
	m_PhysicsWorld->DebugDraw();
	
	ShMgr.m_Ls.Flash();
	ShMgr.m_Ms.m_InstSMeshRenderer->Flash();
	ShMgr.m_Ms.m_SkinMeshRenderer->Flash();
	ShMgr.m_Ms.m_SMeshRenderer->Flash();
	
}
