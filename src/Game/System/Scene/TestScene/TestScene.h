#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "Game/Camera/GameCamera.h"

class TestScene : public SceneBase
{
public:
	virtual ~TestScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

public:
	// その他
	ZAVector<ZSP<ECSEntity>> m_Entities; // エンティティハンドル

	// 平行光源
	ZSP<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;

	// テクスチャ
	ZSP<ZTexture> m_texBack;
	
	ZSP<ZPhysicsWorld> m_PhysicsWorld;

};

#endif