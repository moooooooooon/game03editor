#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSConponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"
#include "EditScene.h"

void EditScene::Release() {

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();

}

void EditScene::Init() {

	//システム準備
	m_UpdateSystems.AddSystem(Make_Shared(AddColliderSystem, appnew));
	//m_UpdateSystems.AddSystem(std::make_shared<AnimationUpdateSystem>());
	//	m_UpdateSystems.AddSystem(std::make_shared<PhysicsSystem>(m_PhysicsWorld));
	//	m_UpdateSystems.AddSystem(std::make_shared<CharaDebugSystem>());
	m_DrawSystems.AddSystem(Make_Shared(TestDrawSystem,appnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,appnew));
	m_DrawSystems.AddSystem(Make_Shared(ColliderDrawSystem, appnew));
	m_DrawSystems.AddSystem(Make_Shared(LineDrawSystem, appnew));
}

void EditScene::Update() {
	DW_STATIC(1, "PerformanceTest_Scene");

	if (INPUT.KeyEnter(VK_ESCAPE)) {
		APP.ExitGameLoop();
		return;
	}
	// 当たり判定の準備
	APP.m_ColEng.ClearList();


	EditSystem.Update();

	ECS.UpdateSystems(m_UpdateSystems, APP.m_DeltaTime);
	APP.m_ColEng.Run();


	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());
}

void EditScene::Draw() {

	//	3D描画開始
	Renderer.Begin3DRendering();
	{


		// 半透明モード
		ShMgr.m_bsAlpha.SetState();

		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();

		// カメラやライトのデータをシェーダ側に転送する
		{
			// カメラ設定& シェーダに書き込み
			EditSystem.GetCamera()->SetCamera();
			// ライト情報をシェーダ側に書き込む
			ShMgr.m_LightMgr.Update();
			//	モデルシェーダの固定定数を書き込み
			ShMgr.m_Ms.SetConstBuffers();
		}

		// [2D]背景描画
		{
			EditSystem.DrawSky();
		}

		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

		EditSystem.Draw();

		APP.m_ColEng.DebugDraw(1);



		//	3Dエフェクト描画

		// ~~~~~~
	}	//	3D描画終了
	Renderer.End3DRendering();

	//	ポストエフェクト
	EditSystem.GetPostEffect().Execute();

	//	2D描画(UIなど)


}


void EditScene::ImGuiUpdate() {
	EditSystem.ImGui();
}
