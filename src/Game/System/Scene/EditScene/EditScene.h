#pragma once

#include "Game/Camera/GameCamera.h"

class EditScene : public SceneBase {
public:
	virtual ~EditScene() {
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();


	// ImGui更新
	virtual void ImGuiUpdate()override;
private:
	void ImGui();
public:

};
