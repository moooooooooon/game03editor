#ifndef __COMMON_COMPONENTS_H__
#define __COMMON_COMPONENTS_H__

#include "ZECS/ECSComponent.h"


// 移動量コンポーネント
DefComponent(VelocityComponent)
{
	ZVec3 Velocity;
};

// ゲーム用モデルコンポーネント
DefComponent(GameModelComponent)
{
	ZSP<ZGameModel>				Model;
	ZSP<RenderState>	RenderFlg;

#if _EDIT
	ZSP<Collider_Mesh>			HitObj;	//	コライダーコンポーネントなくても判定できるように(エディタ用)
#endif

	GameModelComponent() {
		m_Name = "GameModel";
		HitObj = Make_Shared(Collider_Mesh, appnew);
		HitObj->Init(0, -1, 0);
	}
	virtual void Init(){
		RenderFlg = Make_Shared(RenderState, appnew);
	}
	virtual void InitFromJson(const json11::Json& jsonObj);
	void serialize(cereal::JSONOutputArchive& ar) override {
		if (m_OptimizeExportFlg) {
			string fName = m_Name.c_str();
			ar(cereal::make_nvp("ComponentType", fName));
		}
		if (Model) {
			Model->serialize(ar);
		}
		else {
			ar(cereal::make_nvp("ModelFileName", "EmptyModel"));
		}

		RenderFlg->serialize(ar);
	}

	void ImGui();

};

// 単一メッシュコンポーネント
DefComponent(SingleModelComponent)
{
	ZSP<ZSingleModel> Model;
};

// ゲーム用モデルのボーンコントローラーコンポーネント
DefComponent(ModelBoneControllerConponent)
{
	ModelBoneControllerConponent() {
		m_Name = "ModelBoneController";
	}
	ZSP<ZBoneController> BoneController;

	void serialize(cereal::JSONOutputArchive& ar)override {
		if (m_OptimizeExportFlg) {
			std::string name = m_Name.c_str();
			ar(cereal::make_nvp("ComponentType", name));
		}
	}
};

// アニメーターコンポーネント
DefComponent(AnimatorComponent)
{
	ZSP<ZAnimator> Animator;
	ZString InitAnimeName;
	std::function<void(ZAnimeKey_Script*)>ScriptProc;


	/*-------------------------------------------------------------------------------------*/
	bool Enable = false;
	bool InitAnimeLoop   = false;
	/*-------------------------------------------------------------------------------------*/

	AnimatorComponent() {
		m_Name = "Animator";
	}
	virtual void Init() {
		Animator = Make_Shared(ZAnimator, appnew);
		Animator->Init();
	}
	virtual void InitFromJson(const json11::Json& jsonObj);
	void ImGui();

	void serialize(cereal::JSONOutputArchive& ar)override {
		if (m_OptimizeExportFlg) {
			std::string name = m_Name.c_str();
			ar(cereal::make_nvp("ComponentType", name));
		}
		std::string anime = InitAnimeName.c_str();
		ar(cereal::make_nvp("InitAnimeName", anime));
		ar(cereal::make_nvp("InitAnimeLoop", InitAnimeLoop));
		ar(cereal::make_nvp("Enable", Enable));
	}
};


// 当たり判定
DefComponent(ColliderComponent) {
	ColliderComponent() {
		m_Name = "Collider";
		BoxMagnification.Set(1.f,1.f,1.f);
		SphereMagnification = 1.f;
	}
	ZSP<ZGameModel>		HitModel;
	ZSP<Collider_Mesh>	HitObj;

	ZSP<Collider_Sphere> HitSphere;
	float				 SphereMagnification = 1.f;	//	球の倍率


	ZSP<Collider_Box> HitBox;
	ZVec3			  BoxMagnification;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
	virtual void Init()override;
	void ImGui();
	void serialize(cereal::JSONOutputArchive& ar) override {
		if (m_OptimizeExportFlg) {
			std::string name = m_Name.c_str();
			ar(cereal::make_nvp("ComponentType", name));
		}
		if (HitModel) {
			HitModel->serialize(ar);
		}
		else {
			std::string s = "EmptyModel";
			ar(cereal::make_nvp("ModelFileName", s));
		}

		if (m_OptimizeExportFlg) {
			int hitGroups = HitObj->m_DefFilter;
			static int check[] = {
				HitGroups::_0,
				HitGroups::_1,
				HitGroups::_2,
				HitGroups::_3,
				HitGroups::_4,
				HitGroups::_5,
				HitGroups::_6,
				HitGroups::_7,
				HitGroups::_8,
				HitGroups::_9,
				HitGroups::_10,
				HitGroups::_11,
				HitGroups::_12,
				HitGroups::_13,
				HitGroups::_14,
				HitGroups::_15,
			};
			std::vector<int> flgs;
			for (int i = 0; i < sizeof(check) / sizeof(int); i++) {
				if ((hitGroups & check[i]) == 0)continue;
				flgs.push_back(i);
			}
			ar(cereal::make_nvp("HitGroups", flgs));
		}
		else {
			ar(cereal::make_nvp("HitGroups", HitObj->m_DefFilter));
		}

		ar(cereal::make_nvp("SphereMagnification", SphereMagnification));
	}

	//	エンティティの行列が変更されたときに更新(主にBOXで使用)
	void UpdateTransform();

};


#endif