#include "MainFrame/ZMainFrame.h"/*
#include "Game/System/CommonECSComponents/CommonComponents.h"*/

#include "CommonComponents.h"

void GameModelComponent::InitFromJson(const json11::Json & jsonObj) {
	// モデル読み込み
	Model = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());

	// レンダーフラグ
	RenderFlg					= Make_Shared(RenderState, appnew);
	RenderFlg->Character		= jsonObj["Character"].bool_value();
	RenderFlg->SemiTransparent	= jsonObj["SemiTransparent"].bool_value();
	RenderFlg->BaseFrustumOut	= jsonObj["BaseFrustumOut"].bool_value();
	RenderFlg->NoneShadowRender = jsonObj["NoneShadowRender"].bool_value();
	RenderFlg->XRayCol.Set(jsonObj["XRayCol"]);
	if (!jsonObj["DestoryObject"].is_null()) {
		RenderFlg->DestoryObject = jsonObj["DestoryObject"].bool_value();
	}
	if (!jsonObj["FrustumCheck"].is_null()) {
		RenderFlg->FrustumCheck = jsonObj["FrustumCheck"].bool_value();
	}
}

void GameModelComponent::ImGui()
{
	ImGui::Text(m_Name.c_str());

	static ImGuiComboFlags flags = 0;
	
	ZString NowSelect = LevelEditor::EmptyString;
	if (Model) { NowSelect = Model->GetFullFileName(); }

	if (ImGui::BeginCombo("SelectModel", NowSelect.c_str(), flags))
	{
		for (auto& model : EditSystem.ModelList)
		{
			bool is_selected;
			if (!Model) { is_selected = false; }
			else { is_selected = (Model->GetFullFileName() == model.first); }

			if (ImGui::Selectable(model.first.c_str(), is_selected))
			{
				NowSelect = model.first;
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}


	RenderFlg->ImGui();


	//	選択がなければ戻る
	if (NowSelect == LevelEditor::EmptyString) { return; }

	//	モデルの切り替え
	if (Model)
	{
		if (Model->GetFullFileName() != NowSelect) 
		{
			Model = EditSystem.ModelList[NowSelect].model;
		}
	}
	else 
	{
		Model = EditSystem.ModelList[NowSelect].model;
	}
	
}

void ColliderComponent::InitFromJson(const json11::Json & jsonObj) {
	// 当たり判定用モデル
	HitModel = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());

	// 当たり判定マスク
	int hitGroups = 0;
	auto ary = jsonObj["HitGroups"].array_items();
	for (auto& n : ary) {
		hitGroups |= 1 << n.int_value();
	}
	HitObj = Make_Shared(Collider_Mesh, appnew);

	// 基本設定
	HitObj->Init(0,
		hitGroups,
		0xFFFFFFFF, // 形状マスク
		hitGroups // 判定される側時のマスク
	);

	// ColliderComponent情報を仕込ませておく
	//HitObj->m_UserMap["ColliderComp"] = HitObj;
	// メッシュを追加   
	HitObj->AddMesh(HitModel);

	if (jsonObj["SphereMagnification"].is_null()) {
		SphereMagnification = 1.f;
	}
	else {
		SphereMagnification = jsonObj["SphereMagnification"].number_value();
	}
}

void ColliderComponent::Init(){

	HitModel= Make_Shared(ZGameModel, appnew);	
	HitObj	= Make_Shared(Collider_Mesh, appnew);
	
	HitSphere	= Make_Shared(Collider_Sphere, appnew);
	HitBox		= Make_Shared(Collider_Box, appnew);
	

	auto model=m_Entity->GetComponent<GameModelComponent>();
	if (model) {
		if (model->Model) {
			HitModel = model->Model;
			HitSphere->Set(m_Entity->GetComponent<TransformComponent>()->Transform.GetPos(), HitModel->GetBSRadius());					//	エンティティ座標を使用
			SphereMagnification = 1.f;
			HitBox->Set(HitModel->GetAABB_Center(), HitModel->GetAABB_HalfSize(), m_Entity->GetComponent<TransformComponent>()->Transform);	//	エンティティの行列を使用
			BoxMagnification.Set(1.f, 1.f, 1.f);
		}
	}
}

void ColliderComponent::UpdateTransform() {
	if (HitSphere) {
		HitSphere->Set(m_Entity->GetComponent<TransformComponent>()->Transform.GetPos(), HitModel->GetBSRadius()*SphereMagnification);
	}
	else {
		HitSphere = Make_Shared(Collider_Sphere, appnew);
		HitSphere->Set(m_Entity->GetComponent<TransformComponent>()->Transform.GetPos(), HitModel->GetBSRadius()*SphereMagnification);	//	補正した半径を代入
	}

	if (HitModel) {
		ZVec3 Half = HitModel->GetAABB_HalfSize()*BoxMagnification;
		if (HitBox) {
			HitBox->Set(HitModel->GetAABB_Center(), Half, m_Entity->GetComponent<TransformComponent>()->Transform);
		}
		else {
			HitBox = Make_Shared(Collider_Box, appnew);
			HitBox->Set(HitModel->GetAABB_Center(), Half, m_Entity->GetComponent<TransformComponent>()->Transform);	//	エンティティの行列を使用
		}
	}
}

void ColliderComponent::ImGui() {


	ZString NowSelect = LevelEditor::EmptyString;
	if (HitModel) { NowSelect = HitModel->GetFullFileName(); }

	if (ImGui::BeginCombo("ColliderModels", NowSelect.c_str(), 0)) {
		for (auto& model : EditSystem.ModelList) {
			bool is_selected;
			if (!HitModel) { is_selected = false; }
			else { is_selected = (HitModel->GetFullFileName() == model.first); }

			if (ImGui::Selectable(model.first.c_str(), is_selected)) {
				NowSelect = model.first;
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}

	//	選択がなければ戻る
	if (NowSelect == LevelEditor::EmptyString) { return; }

	if (HitModel) {
		if (HitModel->GetFullFileName() != NowSelect) {
			HitModel = EditSystem.ModelList[NowSelect].model;
		}
	}
	else {
		HitModel = EditSystem.ModelList[NowSelect].model;
	}




	int hitGroups = HitObj->m_DefFilter;
	static int check[] = {
		HitGroups::_0,
		HitGroups::_1,
		HitGroups::_2,
		HitGroups::_3,
		HitGroups::_4,
		HitGroups::_5,
		HitGroups::_6,
		HitGroups::_7,
		HitGroups::_8,
		HitGroups::_9,
		HitGroups::_10,
		HitGroups::_11,
		HitGroups::_12,
		HitGroups::_13,
		HitGroups::_14,
		HitGroups::_15,
	};

	std::array<bool, sizeof(check) / sizeof(int)> flgs;
	for (int i = 0; i<sizeof(check)/ sizeof(int); i++) {
		flgs[i] = (hitGroups & check[i]) ? true : false;
	}

	ImGui::BeginChild("HitGroups", ImVec2(ImGui::GetWindowContentRegionWidth(), 50), false, ImGuiWindowFlags_HorizontalScrollbar);
	for (int i = 0; i < sizeof(check)/ sizeof(int); i++) {
		std::string tag = "_" + std::to_string(i);
		ImGui::Checkbox(tag.c_str(),&flgs[i]);
	}
	ImGui::EndChild();

	HitObj->m_DefFilter = 0;

	int i = 0;
	for (auto n : flgs) {
		if (n) {
			HitObj->m_DefFilter |= 1 << i;
		}
		i++;
	}


	//	コリジョン倍率
	static float MinMagnification = 0.001f;
	static float MaxMagnification = 5.f;
	//	球倍率
	if (HitSphere) {
		std::string sphere_string = "SphereRadius:" + std::to_string(HitSphere->m_Sphere.Radius);
		ImGui::Text(sphere_string.c_str());
		if (ImGui::DragFloat("ColliderSphereMagnification", &SphereMagnification, MinMagnification, MinMagnification, MaxMagnification)) {
			HitSphere->SetRadius(HitModel->GetBSRadius()*SphereMagnification);
		}
	}



}

void AnimatorComponent::InitFromJson(const json11::Json & jsonObj) {
	Animator = Make_Shared(ZAnimator, appnew);

	InitAnimeName = jsonObj["InitAnimeName"].string_value().c_str();

	if (!jsonObj["Enable"].is_null()) {
		Enable = jsonObj["Enable"].bool_value();
		InitAnimeLoop = jsonObj["InitAnimeLoop"].bool_value();
	}
	ScriptProc = nullptr;
}

void AnimatorComponent::ImGui() {
	ZString NowSelect = LevelEditor::EmptyString;

	if (InitAnimeName != "") {

		NowSelect = InitAnimeName;
	}

	auto model = m_Entity->GetComponent<GameModelComponent>();
	if (model) {
		if (!model->Model) {
			ImGui::Text("Model is Empty");
			return;
		}
		else {
			if (model->Model->GetAnimeList().empty()) {
				ImGui::Text("AnimeList is Empty");
				return;
			}
		}
	}
	else{
		ImGui::Text("ModelComponent is Empty");
		return;
	}

	if (ImGui::BeginCombo("InitAnime", NowSelect.c_str(), 0)) {

		for (auto& anime : model->Model->GetAnimeList()) {
			bool is_selected;
			if (InitAnimeName == "") { is_selected = false; }
			else {
				is_selected = (InitAnimeName == anime->m_AnimeName);
			}

			if (ImGui::Selectable(anime->m_AnimeName.c_str(), is_selected)) {
				NowSelect = anime->m_AnimeName;
			//	InitAnimeName = NowSelect;
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}

	ImGui::Checkbox("Enable", &Enable);
	ImGui::Checkbox("InitAnimeLoop", &InitAnimeLoop);

	//	選択がなければ戻る
	if (NowSelect == LevelEditor::EmptyString) { return; }

	//	モデルの切り替え
	InitAnimeName = NowSelect;

}
