#ifndef __SKIN_MESH_RENDERER_H__
#define __SKIN_MESH_RENDERER_H__

#include "ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class SkinMeshRenderer : public ModelRendererBase<ZGameModel,ZBoneController, RenderState>
{
public:
	~SkinMeshRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	// 解放
	virtual void Release() override;
	
	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, ZGameModel* model,ZBoneController* bc, RenderState* flg) override;
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() override;


	void Shadow();

	void Z_Prepass();

private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers() override;


	void SetMatrix(const ZMatrix& mat)
	{
		m_cb1_PerObject.m_Data.mW = mat;
		m_cb1_Matrix.m_Data.m_Trans = mat;
	}

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;
	ZConstantBuffer<cbPerObject_MatrixOnly> m_cb1_Matrix;

	//	
	ZSP<ZVertexShader>	m_VS_Shadow;
	ZSP<ZPixelShader>	m_PS_Shadow;
	ZSP<ZPixelShader>	m_PS_Chara;

	ZSP<ZPixelShader> m_PsMono;
};

#endif

