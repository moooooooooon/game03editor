#include "MainFrame/ZMainFrame.h"

bool ColliderRenderer::Init(const ZString& vsPath, const ZString& psPath) {
	Release();

	bool successed = true;

	m_VS = APP.m_ResStg.LoadVertexShader(vsPath, ZVertex_Pos_UV_TBN::GetVertexTypeData());
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);


	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb1_Matrix.Create(1);

	return successed;
}

void ColliderRenderer::Release() {
	if (m_VS)m_VS->Release();
	if (m_PS)m_PS->Release();


	m_cb1_PerObject.Release();
	m_cb1_Matrix.Release();
}

void ColliderRenderer::Submit(ZMatrix* mat, ZSingleModel* pModel) {
}

void ColliderRenderer::Submit(ZMatrix* mat, ZGameModel* pModel) {
	if (!pModel) { return; }
	if (pModel->GetModelTbl_Static().size()) {
		for (auto sModel : pModel->GetModelTbl_Static()) {
			m_RenderBuffer[std::make_tuple(sModel.GetPtr())].push_back(mat);
		}
	}
	else {
		for (auto sModel : pModel->GetModelTbl_Skin()) {
			m_RenderBuffer[std::make_tuple(sModel.GetPtr())].push_back(mat);
		}
	}
}


void ColliderRenderer::Flash() {
	if (m_RenderBuffer.size() <= 0)
		return;

	m_VS->SetShader();
	m_PS->SetShader();

	SetContactBuffers();

	for (auto& param : m_RenderBuffer) {
		ZSingleModel* pModel = std::get<0>((param).first);

		if (pModel == nullptr)
			continue;

		// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		for (UINT i = 0; i < materials.size(); i++) {
			if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
				continue;

			for (auto& mat : param.second) {
				if (mat == nullptr)
					continue;

				// 行列セット
				SetMatrix(*mat);
				m_cb1_PerObject.WriteData();
				pModel->GetMesh()->DrawSubset(i);
			}
		}

	}

	m_RenderBuffer.clear();
}

void ColliderRenderer::SetContactBuffers() {
	// オブジェクト単位のデータ
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();

}