#ifndef __COLLIDER_RENDERER_H__
#define __COLLIDER_RENDERER_H__

#include "../ModelRenderer/ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class ColliderRenderer : public ModelRendererBase<ZSingleModel> {
public:
	virtual ~ColliderRenderer() {
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	// 解放
	virtual void Release()override;

	// 描画
	virtual void Submit(ZMatrix* mat, ZSingleModel* pModel);
	void Submit(ZMatrix* mat, ZGameModel* pModel);
	virtual void Flash() override;

#pragma region RenderParam

	void SetMulColor(const ZVec4* color = &ZVec4::one) {
		if (color == nullptr)
			m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
		else
			m_cb1_PerObject.m_Data.MulColor = *color;
	}

#pragma endregion

private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers()override;

	void SetMatrix(const ZMatrix& mat) {
		m_cb1_PerObject.m_Data.mW = mat;
		m_cb1_Matrix.m_Data.m_Trans = mat;
	}


private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;

	ZConstantBuffer<cbPerObject_MatrixOnly> m_cb1_Matrix;
};
#endif