#ifndef __RENDER_STATE_H__
#define __RENDER_STATE_H__

struct RenderState
{
	bool SemiTransparent	= false;	//	trueなら半透明部分が存在するオブジェクトなのでBasePassで描画しない
	bool Character			= false;	//	trueならプレイアブルキャラクターなので、X-Passを実行する
	bool BaseFrustumOut		= false;	//	trueならベースパスの視錐台の外なのでシャドウ以外は描画しない
	bool NoneShadowRender	= false;	//	trueならBasePass内でピクセルシェーダを切り替えて、シャドウマップの計算を行わない(軽量化フラグで例えば遠景のオブジェクトに使用)
	bool FrustumCheck		= true;		//	視錐台チェックをするかどうか
	bool DestoryObject		= true;		//	消せるオブジェクトかどうか
	
//	デバッグ用なので悪しからず
	bool DebugOutLine		= false;
	ZVec4 OutLineCol;

	ZVec4 XRayCol;
	//

	void serialize(cereal::JSONOutputArchive& ar) {
		ar(cereal::make_nvp("SemiTransparent", SemiTransparent));
		ar(cereal::make_nvp("Character", Character));
		ar(cereal::make_nvp("BaseFrustumOut", BaseFrustumOut));
		ar(cereal::make_nvp("NoneShadowRender", NoneShadowRender));
		ar(cereal::make_nvp("FrustumCheck", FrustumCheck));
		ar(cereal::make_nvp("DestoryObject", DestoryObject));
		{
			std::vector<float> v;
			v.push_back(XRayCol.x);
			v.push_back(XRayCol.y);
			v.push_back(XRayCol.z);
			v.push_back(XRayCol.w);
			ar(cereal::make_nvp("XRayCol", v));
		}
	}

	void ImGui() {
		ImGui::Checkbox("SemiTransparent", &SemiTransparent);
		ImGui::Checkbox("NoneShadowRender", &NoneShadowRender);
		ImGui::Checkbox("FrustumCheck", &FrustumCheck);
		ImGui::Checkbox("DestoryObject", &DestoryObject);
		ImGui::ColorEdit4("X-Ray_Color", XRayCol);
	}

	RenderState& operator= (const RenderState& V) {
		SemiTransparent = V.SemiTransparent;
		Character		= V.Character; 
		BaseFrustumOut	= V.BaseFrustumOut;
		NoneShadowRender = V.NoneShadowRender;
		return *this; 
	}
};

#endif