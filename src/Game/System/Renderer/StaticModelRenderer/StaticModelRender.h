#ifndef __STATIC_MODEL_RENDERER_H__
#define __STATIC_MODEL_RENDERER_H__

#include "../ModelRenderer/ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class StaticModelRenderer : public ModelRendererBase<ZSingleModel, RenderState>
{
public:
	virtual ~StaticModelRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	// 解放
	virtual void Release()override;

	// 描画
	virtual void Submit(ZMatrix* mat, ZSingleModel* pModel, RenderState* flgs) override;
	void Submit(ZMatrix* mat, ZGameModel* pModel, RenderState* flgs);
	virtual void Flash() override;

	void Shadow();

	void Z_Prepass();

	void OutLine();

#pragma region RenderParam

	// ライティング ON/OFF
	void SetLightEnable(bool flag)
	{
		m_cb1_PerObject.m_Data.LightEnable = flag ? 1 : 0;
	}

	void SetDistanceFog(bool flag, const ZVec3* fogColor = nullptr, float fogDensity = -1)
	{
		m_cb1_PerObject.m_Data.DistanceFogDensity = flag ? 1.0f : 0.0f;
		if (fogColor)
			m_cb1_PerObject.m_Data.DistanceFogColor = *fogColor;
		if (fogDensity > 0)
			m_cb1_PerObject.m_Data.DistanceFogDensity = fogDensity;
	}

	void SetMulColor(const ZVec4* color = &ZVec4::one)
	{
		if (color == nullptr)
			m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
		else
			m_cb1_PerObject.m_Data.MulColor = *color;
	}

#pragma endregion


private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetContactBuffers()override;

	void SetMatrix(const ZMatrix& mat)
	{
		m_cb1_PerObject.m_Data.mW = mat;
		m_cb1_Matrix.m_Data.m_Trans = mat;
	}

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);


	void RenderProc(ZAMap <std::tuple<ZSingleModel*, RenderState*>, ZAVector<ZMatrix*>>& map, bool clearBuffer = false);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;

	ZConstantBuffer<cbPerObject_MatrixOnly> m_cb1_Matrix;

	//	
	ZSP<ZVertexShader>	m_VS_Shadow;
	ZSP<ZPixelShader>	m_PS_Shadow;

	ZSP<ZPixelShader>	m_PsMono;

	ZSP<ZVertexShader>	m_VS_OutLine;

	//	半透明オブジェクト用
	ZAMap <std::tuple<ZSingleModel*, RenderState*>, ZAVector<ZMatrix*>> m_SemiTransparentBuffer;
};

#endif