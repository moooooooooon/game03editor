#ifndef __MAPOBJECT_COMPONENTS_H__
#define __MAPOBJECT_COMPONENTS_H__

// セキュリティ
DefComponent(SecurityComponent) {
	bool Enable;
	int Gage;

	SecurityComponent() {
		m_Name = "Security";
	}
	virtual void InitFromJson(const json11::Json& jsonObj)override;

	void serialize(cereal::JSONOutputArchive& ar) override {

		string fName = m_Name.c_str();
		ar(cereal::make_nvp("ComponentType", fName));
		ar(cereal::make_nvp("Gage", Gage));
		ar(cereal::make_nvp("Enable", Enable));
	}

	void ImGui();
};

// サーバー
DefComponent(ServerComponent) {
	bool Enable;
	int Gage;
	ServerComponent() {
		m_Name = "Server";
	}
	virtual void InitFromJson(const json11::Json& jsonObj)override;

	void serialize(cereal::JSONOutputArchive& ar) override {

		string fName = m_Name.c_str();
		ar(cereal::make_nvp("ComponentType", fName));
		ar(cereal::make_nvp("Gage", Gage));
		ar(cereal::make_nvp("Enable", Enable));
	}

	void ImGui();
};


#endif