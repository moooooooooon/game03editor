#include "MainFrame/ZMainFrame.h"
#include "MapObjectComponents.h"

void SecurityComponent::InitFromJson(const json11::Json & jsonObj) {
	Enable = true;
	Gage = jsonObj["Gage"].int_value();
}

void SecurityComponent::ImGui() {
	ImGui::Checkbox("Security_Enable", &Enable);
	ImGui::SliderInt("Security_Gage", &Gage, 0, 100);
}

void ServerComponent::InitFromJson(const json11::Json & jsonObj) {
	Enable = true;
	Gage = jsonObj["Gage"].int_value();
}

void ServerComponent::ImGui() {
	ImGui::Checkbox("Server_Enable", &Enable);
	ImGui::SliderInt("Server_Gage", &Gage, 0, 100);
}
