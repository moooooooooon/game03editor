#pragma once

struct GameModelComponent;


class LevelEditor {
	/*========================================================================*/
	//	Public Functions
	/*========================================================================*/
public:
	LevelEditor()  {}
	~LevelEditor() {
		Release();
	}

	void Init();
	void Release();

	void Update();

	void ImGui();

	void LoadModels(const ZString& _BaseDirectory);
	
	void ShowSelectEntity();
	void ShowLevelMainPanel();




	/*========================================================================*/
	//	Public Variables
	/*========================================================================*/
public:
	static const ZString				EmptyString;




	/*========================================================================*/
	//	Private Functions
	/*========================================================================*/
private:
	bool LoadInitData();
	void LoadLevelObject(const ZString& fileName);
	void SelectLoadFile();
	void SaveInitData();
	void SaveLevelObject(const ZString& fileName);
	void OptimizeSaveLevelObject(const ZString& fileName);	//	出力データをある程度最適化して書き出す(エディタで読み込めなくなるので注意)
	void SelectSaveFile(bool optimize = false);
	void LoadModel();
	void AddObject();
	void RemoveObject();
	void EditTransform(const float *cameraView, float *cameraProjection, float* matrix);
	void EditTransform_InBulk(const float *cameraView, float *cameraProjection);

	void ShowModelNameList();
	void ShowModelNameCombo();

	void SelectObject();

	void CreateObjects_A(json11::Json& json);
	void CreateObjects_B(json11::Json& json);
	//void CreateObjects_C(json11::Json& json);
	//void CreateObjects_D(json11::Json& json);

	void AddComponent(ZString& compName);
	void AddComponentButton();
	ZSP<ECSEntity> CreateEntityFromJson(const json11::Json & jsonObj);

	//	Copy&Paste
	void CopyObjects();
	void PasteObjects();
	void ClearClipObjects();

	bool AddAnimatorCompInit(InstanceData& inst);

	void AddSelectObject();
	void ClearAddSelectObject();
	/*========================================================================*/
	//	Private Variables
	/*========================================================================*/
private:

	//	Data
	vector<string>					m_AddFileName;

	//	Current Data
	ZString							m_SelectModelName;		//	モデルの追加などで選択するファイル名

	//	Init Info
	size_t							AutoLoadXedFileMaxSize;

	//	Save Info
	ZString							m_SaveFileName;

	//	TransformEditorParams
	ImGuizmo::OPERATION				m_CurrentGizmoOperation;
	ImGuizmo::MODE					m_CurrentGizmoMode;
	
	struct ManipulateConfig {

		ZVec3 SnapTranslation;
		ZVec3 SnapRotation;
		ZVec3 SnapScale;

		ManipulateConfig() {
			SnapTranslation.Set(0.01f, 0.01f, 0.01f);
			SnapRotation.Set(0.01f, 0.01f, 0.01f);
			SnapScale.Set(0.01f, 0.01f, 0.01f);
		}
	};
	ManipulateConfig				m_ManipulateConfig;

	//	複数選択のオブジェクトをまとめて動かす際のデータ
	struct InBulkMoveData {
		InBulkMoveData() {
		//	CenterMatrix.CreateIdentity();
			BeforMatrix.CreateIdentity();
		}
		ZMatrix		BeforMatrix;
		bool		UpdateMatrix = false;
		//...
		//...
	};
	InBulkMoveData					m_InBulkDat;


	//struct ClipData {
	//	ZVec3	FromCenteVec;		//	中心点からオブジェクト座標へのベクトル

	//};
	//vector<AddSelectEntityData>		m_ClipData;		//	オブジェクトのコピペ用

	//vector<AddSelectEntityData>		m_ClipData;		//	オブジェクトのコピペ用

	//	Editor Params
	static const ZString			initFile;
	static const ZString			DefaultSaveFile;
	static const float				EditorVersion;


};
