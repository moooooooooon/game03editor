#include "MainFrame/ZMainFrame.h"
#include "Game/Camera/GameCamera.h"

#include "Game/System/CommonECSComponents/CommonComponents.h"
#include "Game/System/TestECSConponents/TestComponents.h"
#include "Game/System/TestECSSystems/TestSystems.h"

#include "Game/System/CollisionEngine/CollisionEngine.h"

namespace fSystem = experimental::filesystem;
const ZString LevelEditor::initFile			= "LevelEditor.ini";
const ZString LevelEditor::DefaultSaveFile	= "LevelObject.json";
const float	 LevelEditor::EditorVersion		= 2.0f;
const ZString LevelEditor::EmptyString		= "Empty";


void LevelEditor::Init() {

	if (!LoadInitData()) { APP.ExitGameLoop(); }	//	エラーならプログラム終了
	LoadModels("data");
	
	//	セーブファイル初期化(Ctrl+s操作で保存対象があるように)
	m_SaveFileName		= DefaultSaveFile;
	m_SelectModelName	= EmptyString;

	//	移動設定初期化
	m_CurrentGizmoOperation	= ImGuizmo::TRANSLATE;
	m_CurrentGizmoMode		= ImGuizmo::LOCAL;


	//	空オブジェクトを作成
	E_CurrentEntity = EditSystem.CreateEmptyModelEntity();


//	LoadLevelObject("CheckModels.json");
}

bool LevelEditor::LoadInitData() {

	string error;
	json11::Json json = LoadJsonFromFile(initFile.c_str(), error);

	if (error.size() != 0) {
		MessageBox(APP.m_Window->GetWindowHandle(), "LevelEditorのInitFileが適切な場所に存在しません", "Error", MB_OK);
		return false;
	}


	AutoLoadXedFileMaxSize	= json["MaxDataSize"].int_value();
	for (auto& add:json["AddLoadFiles"].array_items()) {
		m_AddFileName.push_back(add.string_value().c_str());
	}

	return true;
}

void LevelEditor::Release() {

	if (EditSystem.ModelList.size()) {
		SaveInitData();
	}

	E_CurrentEntity = nullptr;
}

void LevelEditor::Update() {
	bool stayShift = false;
	bool stayCtrl  = false;


	if (INPUT.KeyStay(VK_SHIFT))	{ stayShift = true; }
	if (INPUT.KeyStay(VK_CONTROL))	{ stayCtrl	= true; }

	if (INPUT.KeyEnter('S')) {
		if (stayCtrl) {	//	Shortcut Save
			if (MessageBox(APP.m_Window->GetWindowHandle(), TEXT("上書き保存します\n保存ファイルが選択されていない場合は\nLevelObject.jsonが作成されます"), "確認", MB_YESNO) == IDYES) {
				SaveLevelObject(m_SaveFileName);
			}
		}
	}

	if (INPUT.KeyEnter('R')) {
		if (stayCtrl) {	//	Shortcut Remove
			//	追加選択があるときは実行させない
			if (E_AddSelectEntitys.empty()) {
				RemoveObject();
			}
		}
	}

	if (INPUT.KeyEnter(VK_LBUTTON)) {

		if (stayShift)
		{ //	Shortcut Add
		//	AddObject();
		}
		else if(stayCtrl)
		{
			//	追加選択があるときは実行させない
			if (E_AddSelectEntitys.empty()) {
				AddObject();
			}
		}
		else
		{
			//　マニピュレータに使用するので使用しない
		}
	}


	if (INPUT.KeyEnter(VK_RBUTTON)) 
	{
		if (stayShift) 
		{
			SelectObject();
		}
		else if (stayCtrl) 
		{
			AddSelectObject();
		}
		else 
		{
			//	カメラ回転に使うので使用しない
		}
	}


	if (INPUT.KeyStay(VK_SPACE)) { ClearAddSelectObject(); }
	
	if (INPUT.KeyStay('Z')) {
		m_CurrentGizmoOperation = ImGuizmo::TRANSLATE;
	}
	if (INPUT.KeyStay('X')) {
		m_CurrentGizmoOperation = ImGuizmo::ROTATE;
	}
	if (INPUT.KeyStay('C')) {
		if (E_AddSelectEntitys.empty()) {
			m_CurrentGizmoOperation = ImGuizmo::SCALE;
		}

		if (stayCtrl) {
			CopyObjects();
		}

	}


	//	貼り付け
	if (INPUT.KeyEnter('V')) {
		if (stayCtrl) {
			PasteObjects();
		}
	}



}

void LevelEditor::SaveInitData() {

	m_AddFileName.clear();
	for (auto& dat : EditSystem.ModelList) {
		if (!dat.second.addLoadModel)continue;
		string name = dat.second.model->GetFullFileName().c_str();
		m_AddFileName.push_back(name);
	}

	std::ofstream ss(initFile);
	{
		cereal::JSONOutputArchive o_archive(ss);
		o_archive(cereal::make_nvp("MaxDataSize", AutoLoadXedFileMaxSize));
		o_archive(cereal::make_nvp("AddLoadFileCnt", m_AddFileName.size()));
		o_archive(cereal::make_nvp("AddLoadFiles", m_AddFileName));
	}
}

void LevelEditor::SaveLevelObject(const ZString& fileName) {
	m_SaveFileName = fileName;

	struct Save{
		EntityHandle handle;
		string uuid;
		void serialize(cereal::JSONOutputArchive & ar) {
			std::vector<__ECSComponentBase*> list;
			ECS.GetCompList(handle, list);
			
			ar(cereal::make_nvp("UUID", uuid));

			std::vector<string>	names;
			for (auto& comp : list) {
				string name = comp->m_Name.c_str();
				names.push_back(name);
			}
			ar(cereal::make_nvp("CompList", names));
			
			for (auto& comp : list) {
				string name = comp->m_Name.c_str();
				ar(cereal::make_nvp(name, *comp));
			}
		}
	};

	vector<Save> objs;

	for (auto& obj : EditSystem.GetEntities()) {

		if (obj->GetHandle() == NULL_ENTITY_HANDLE)continue;
		Save add;
		add.handle = obj->GetHandle();

		//	コンポーネントがなければ保存しない
		std::vector<__ECSComponentBase*> list;
		ECS.GetCompList(add.handle, list);
		if(list.empty())continue;

		//	モデルデータがnullならセーブしない
		auto model = ECS.GetComponent<GameModelComponent>(add.handle);
		if (model) {
			if (!model->Model) { continue; }	
		}

		//	UUID
		auto uuid = obj->GetUUID();
		string uuidstr = UUIDToStr(uuid).c_str();
		add.uuid = uuidstr;

		objs.push_back(add);

	}
	std::ofstream ss(m_SaveFileName.c_str());
	{
		cereal::JSONOutputArchive o_archive(ss);
		o_archive(cereal::make_nvp("EditorVersion", EditorVersion));
		o_archive(cereal::make_nvp("Objects", objs));
	}

}

void LevelEditor::OptimizeSaveLevelObject(const ZString& fileName) {
	m_SaveFileName = fileName;

	struct Save {
		EntityHandle handle;
		string uuid;
		void serialize(cereal::JSONOutputArchive & ar) {
			std::vector<__ECSComponentBase*> list;
			ECS.GetCompList(handle, list);
			
			ar(cereal::make_nvp("UUID", uuid));

			std::vector<string>	names;
			for (auto& comp : list) {
				string name = comp->m_Name.c_str();
				names.push_back(name);
			}
			ar(cereal::make_nvp("CompList", names));

			for (auto& comp : list) {
				string name = comp->m_Name.c_str();
				comp->m_OptimizeExportFlg = true;
				ar(cereal::make_nvp(name, *comp));
			}
		}
	};

	vector<Save> objs;

	for (auto& obj : EditSystem.GetEntities()) {

		if (obj->GetHandle() == NULL_ENTITY_HANDLE)continue;
		Save add;
		add.handle = obj->GetHandle();

		//	コンポーネントがなければ保存しない
		std::vector<__ECSComponentBase*> list;
		ECS.GetCompList(add.handle, list);
		if (list.empty())continue;

		//	モデルデータがnullならセーブしない
		auto model = ECS.GetComponent<GameModelComponent>(add.handle);
		if (model) {
			if (!model->Model) { continue; }
		}

		//	UUID
		auto uuid = obj->GetUUID();
		string uuidstr = UUIDToStr(uuid).c_str();
		add.uuid = uuidstr;

		objs.push_back(add);

	}
	std::ofstream ss(m_SaveFileName.c_str());
	{
		cereal::JSONOutputArchive o_archive(ss);
//		o_archive(cereal::make_nvp("EditorVersion", EditorVersion));
		o_archive(cereal::make_nvp("Objects", objs));
	}

}

void LevelEditor::SelectSaveFile(bool optimize) {
	static OPENFILENAME     ofn;
	static TCHAR            szPath[MAX_PATH];
	static TCHAR            szFile[MAX_PATH];

	if (szPath[0] == TEXT('\0')) {
		GetCurrentDirectory(MAX_PATH, szPath);
	}
	if (ofn.lStructSize == 0) {
		ofn.lStructSize		= sizeof(OPENFILENAME);
		ofn.hwndOwner		= APP.m_Window->GetWindowHandle();
		ofn.lpstrInitialDir = szPath;
		ofn.lpstrFile		= szFile;
		ofn.nMaxFile		= MAX_PATH;
		ofn.lpstrDefExt		= TEXT(".txt");
		ofn.lpstrFilter		= TEXT("Jsonファイル(*.json)\0*.json\0テキストファイル(*.txt)\0*.txt\0");	//	保存形式(.txtと.json)
		ofn.lpstrTitle		= TEXT("エディットデータを保存");
		ofn.Flags			= OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	}

	if (GetSaveFileName(&ofn)) {
		if (MessageBox(APP.m_Window->GetWindowHandle(), TEXT("保存しますか"), szFile, MB_YESNO) == IDYES) {
			if (optimize) {
				OptimizeSaveLevelObject(szFile);
			}
			else {
				SaveLevelObject(szFile);
			}
		}
		else {
			MessageBox(APP.m_Window->GetWindowHandle(), TEXT("保存しませんでした"), szFile, MB_OK);
		}
	}
	//	カレントディレクトリをもとに戻す(やらないとimgui.iniが変更したカレントに作られる)
	SetCurrentDirectory(szPath);
}

void LevelEditor::LoadLevelObject(const ZString & fileName) {
	//	先に全オブジェクトを解放
	E_CurrentEntity = nullptr;
	EditSystem.DeleteAllEntity();

	std::string error;
	json11::Json json = LoadJsonFromFile(fileName.c_str(), error);
	if (error.size() != 0) {
		MessageBox(APP.m_Window->GetWindowHandle(), TEXT("読み込み失敗"), fileName.c_str(), MB_OK);
		return;
	}

	//	テストエディタの読み込み(後々出力形式変わった時に読み込めるように)
	if (json["EditorVersion"].number_value() < 2.0) {
		CreateObjects_A(json);
	}
	else if(json["EditorVersion"].number_value() < 3.0){
		CreateObjects_B(json);
	}

}

void LevelEditor::SelectLoadFile() {
	static OPENFILENAME     ofn;
	static TCHAR            szPath[MAX_PATH];
	static TCHAR            szFile[MAX_PATH];

	if (szPath[0] == TEXT('\0')) {
		GetCurrentDirectory(MAX_PATH, szPath);
	}
	if (ofn.lStructSize == 0) {
		ofn.lStructSize		= sizeof(OPENFILENAME);
		ofn.hwndOwner		= APP.m_Window->GetWindowHandle();
		ofn.lpstrInitialDir = szPath;
		ofn.lpstrFile		= szFile;				
		ofn.nMaxFile		= MAX_PATH;
		ofn.lpstrDefExt		= TEXT(".txt");
		ofn.lpstrFilter		= TEXT("Jsonファイル(*.json)\0*.json\0テキストファイル(*.txt)\0*.txt\0");
		ofn.lpstrTitle		= TEXT("エディットデータを読み込み");
		ofn.Flags			= OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	}

	if (GetOpenFileName(&ofn)) {
		SetCurrentDirectory(szPath);
		LoadLevelObject(szFile);
	}

	//	カレントディレクトリをもとに戻す(やらないとimgui.iniが変更したカレントに作られる)
	SetCurrentDirectory(szPath);
}

void LevelEditor::LoadModel() {
	static OPENFILENAME     ofn;
	static TCHAR            szPath[MAX_PATH];
	static TCHAR            szFile[MAX_PATH];

	if (szPath[0] == TEXT('\0')) {
		GetCurrentDirectory(MAX_PATH, szPath);
	}
	if (ofn.lStructSize == 0) {
		ofn.lStructSize		= sizeof(OPENFILENAME);
		ofn.hwndOwner		= APP.m_Window->GetWindowHandle();
		ofn.lpstrInitialDir = szPath;
		ofn.lpstrFile		= szFile;
		ofn.nMaxFile		= MAX_PATH;
		ofn.lpstrDefExt		= TEXT(".txt");
		ofn.lpstrFilter		= TEXT("xedファイル(*.xed)\0");
		ofn.lpstrTitle		= TEXT("Xed追加読み込み");
		ofn.Flags			= OFN_OVERWRITEPROMPT;
	}
	
	if (GetOpenFileName(&ofn)) {

		//	カレントディレクトリをもとに戻す(読み込みパスの都合)
		SetCurrentDirectory(szPath);

		int		cp			= ZString::npos;
		ZString	filepath	= szFile;
		ZString	cfile		= "data";

		cp = filepath.find(cfile);
		
		if (cp > 0) {
			filepath	= filepath.substr(cp);
			auto model	= APP.m_ResStg.LoadMesh(filepath.c_str());
			if (model)
			{
				EditSystem.ModelList[filepath.c_str()].model		= model;
				EditSystem.ModelList[filepath.c_str()].addLoadModel	= true;
			}
		}
		else {
			//	dataディレクトリ以下ではないファイルは読み込まない
			MessageBox(APP.m_Window->GetWindowHandle(), "[data/...]に含まれないファイルです\n読み込みを中止しました\ndataディレクトリ以下にファイル、フォルダを作成してください", "Error", MB_OK);
		}
	}

	//	カレントディレクトリをもとに戻す(やらないとimgui.iniが変更したカレントに作られる)
	SetCurrentDirectory(szPath);
}

void LevelEditor::LoadModels(const ZString & _BaseDirectory) {
	
	fSystem::path p(_BaseDirectory); // 列挙の起点

	auto load_func = [this](const fSystem::path& p)
	{
		ZString path = p.string().c_str();
		//	path = p.filename().ZString();
		if (fSystem::is_regular_file(p)) { // ファイル

			int pos = path.find(".xed");

			if (pos != std::string::npos)
			{
				//	ファイルサイズ取得
				std::ifstream file(path, std::ifstream::ate);
				size_t filesize = static_cast<size_t>(file.tellg());

				if (filesize <= AutoLoadXedFileMaxSize)
				{
					auto model= APP.m_ResStg.LoadMesh(path);
					if (model->GetModelTbl_Skin().size()) { return; }	//	スキンメッシュは読み込まない
					if (model) { EditSystem.ModelList[path.c_str()].model = model; }
				}
			}

		}
		//else if (fSystem::is_directory(p)) { // ディレクトリ
		//}

	};


	std::for_each(fSystem::recursive_directory_iterator(p), fSystem::recursive_directory_iterator(), load_func);

	//	追加読み込み
	for (auto& add : m_AddFileName) {
		auto model = APP.m_ResStg.LoadMesh(add.c_str());
		if (model) {
			EditSystem.ModelList[add.c_str()].model			= model;
			EditSystem.ModelList[add.c_str()].addLoadModel	= true;
		}
	}

}

void LevelEditor::AddObject() {

	//	カレントが変わったら輪郭描画を切る
	if (E_CurrentEntity) {
		//	一応NULLチェック
		if (E_CurrentEntity->GetHandle() != NULL_ENTITY_HANDLE) {
			auto model = E_CurrentEntity->GetComponent<GameModelComponent>();
			if (model) {
				model->RenderFlg->DebugOutLine = false;
			}
		}
	}
	E_CurrentEntity = EditSystem.CreateEmptyModelEntity();

	if (m_SelectModelName != EmptyString) {
		E_CurrentEntity->GetComponent<GameModelComponent>()->Model = EditSystem.ModelList[m_SelectModelName.c_str()].model;
	}

	ZVec3 vec;
	vec		= ZVec3::Front;
	vec.z	= 10.f;
	{
		//if (modelcomp->Model) {
		//	vec.z += modelcomp->Model->GetAABB_HalfSize().z * 1.5f;	//	AABBより少しだけ遠くに配置する
		//	if (vec.Length() > 100.f) {
		//		vec.z = 100.f;
		//	}
		//}
	}

	vec.Transform(EditSystem.GetCamera()->mCam);
	E_CurrentEntity->GetComponent<TransformComponent>()->Transform.CreateMove(vec);

}

void LevelEditor::RemoveObject() {
	if (E_CurrentEntity) {
		E_CurrentEntity->Remove();

		bool is_add = false;
		//	一番最後のエンティティをセット
		for (auto ent : EditSystem.GetEntities()) {
			if (ent->GetHandle() != NULL_ENTITY_HANDLE) {
				E_CurrentEntity = ent;
				is_add	= true;
			}
		}
		//	無ければ空オブジェクト作成
		if (!is_add) {
			m_SelectModelName = EmptyString;
			AddObject();
		}
	}

	////	削除が発生した場合クリップボードのデータを消す(存在し無いエンティティを参照しそうなので)
	//ClearClipObjects();
}

void LevelEditor::EditTransform(const float *cameraView, float *cameraProjection, float* matrix) {
	ZVec3 snap;

	if (ImGui::CollapsingHeader("Transform")) {
		{
			if (ImGui::RadioButton("Translate", m_CurrentGizmoOperation == ImGuizmo::TRANSLATE)) {
				m_CurrentGizmoOperation = ImGuizmo::TRANSLATE;
			}
			ImGui::SameLine();
			if (ImGui::RadioButton("Rotate", m_CurrentGizmoOperation == ImGuizmo::ROTATE)) {
				m_CurrentGizmoOperation = ImGuizmo::ROTATE;
			}
			ImGui::SameLine();
			if (ImGui::RadioButton("Scale", m_CurrentGizmoOperation == ImGuizmo::SCALE)) {
				m_CurrentGizmoOperation = ImGuizmo::SCALE;
			}
		}
		{
			//	
			float matrixTranslation[3], matrixRotation[3], matrixScale[3];
			ImGuizmo::DecomposeMatrixToComponents(matrix, matrixTranslation, matrixRotation, matrixScale);
			ImGui::InputFloat3("Translate", matrixTranslation, 3);
			ImGui::InputFloat3("Rotation", matrixRotation, 3);
			ImGui::InputFloat3("Scale", matrixScale, 3);
			ImGuizmo::RecomposeMatrixFromComponents(matrixTranslation, matrixRotation, matrixScale, matrix);
		}

		{

			switch (m_CurrentGizmoOperation) {
			case ImGuizmo::TRANSLATE:
				snap = m_ManipulateConfig.SnapTranslation;
				ImGui::InputFloat3("Snap", &snap.x);
				m_ManipulateConfig.SnapTranslation = snap;
				break;
			case ImGuizmo::ROTATE:
				snap = m_ManipulateConfig.SnapRotation;
				ImGui::InputFloat("Angle Snap", &snap.x);
				m_ManipulateConfig.SnapRotation = snap;
				break;
			case ImGuizmo::SCALE:
				snap = m_ManipulateConfig.SnapScale;
				ImGui::InputFloat("Scale Snap", &snap.x);
				m_ManipulateConfig.SnapScale = snap;
				break;
			}
		}
	}

	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
	ImGuizmo::Manipulate(cameraView, cameraProjection, m_CurrentGizmoOperation, m_CurrentGizmoMode, matrix, NULL, &snap.x);
}

void LevelEditor::EditTransform_InBulk(const float *cameraView, float *cameraProjection) {

	//	いい感じじゃないので一旦消す
	if (m_CurrentGizmoOperation == ImGuizmo::SCALE) {
		m_CurrentGizmoOperation = ImGuizmo::TRANSLATE;
	}
	if (ImGui::CollapsingHeader("Transform")) {
		{
			if (ImGui::RadioButton("Translate", m_CurrentGizmoOperation == ImGuizmo::TRANSLATE)) {
				m_CurrentGizmoOperation = ImGuizmo::TRANSLATE;
			}
			ImGui::SameLine();
			if (ImGui::RadioButton("Rotate", m_CurrentGizmoOperation == ImGuizmo::ROTATE)) {
				m_CurrentGizmoOperation = ImGuizmo::ROTATE;
			}
			//ImGui::SameLine();
			//if (ImGui::RadioButton("Scale", m_CurrentGizmoOperation == ImGuizmo::SCALE)) {
			//	m_CurrentGizmoOperation = ImGuizmo::SCALE;
			//}
		}
	}


	//	中心行列初期化
	ZVector<ZSP<ECSEntity>>	SelectList;	//	E_AddSelectEntitysにそれぞれのポインタを持たせてもいいが、いちいちエラーチェックが面倒なので
	ZVec3 CenterPos;
	if (E_CurrentEntity->GetComponent<TransformComponent>()) {
		CenterPos = E_CurrentEntity->GetComponent<TransformComponent>()->Transform.GetPos();
	}

	/*============================================*/
	//	中心座標計算
	/*============================================*/
	{
		for (auto ent : EditSystem.GetEntities()) {
			for (auto addselect : E_AddSelectEntitys) {
				if (addselect.Handle == ent->GetHandle()) {
					SelectList.push_back(ent);
					CenterPos += ent->GetComponent<TransformComponent>()->Transform.GetPos();
				}
			}
		}
		if (SelectList.size()) {
			if (E_CurrentEntity->GetComponent<TransformComponent>()) {
				CenterPos /= (float)(SelectList.size() + 1);
			}
			else {
				CenterPos /= (float)SelectList.size();
			}
		}
	}

	ZMatrix NowCenterMatrix;

	NowCenterMatrix.CreateIdentity();
	if (m_InBulkDat.UpdateMatrix) {
		NowCenterMatrix.SetPos(CenterPos);
		m_InBulkDat.UpdateMatrix = false;
	}
	else {
		NowCenterMatrix.SetPos(m_InBulkDat.BeforMatrix.GetPos());
	}


	//	移動前の座標を保存
	ZVec3 old = NowCenterMatrix.GetPos();

	/*============================================*/
	//	親行列更新
	/*============================================*/
	ImGuiIO& io = ImGui::GetIO();
	ImGuizmo::SetRect(0, 0, io.DisplaySize.x, io.DisplaySize.y);
	ImGuizmo::Manipulate(cameraView, cameraProjection, m_CurrentGizmoOperation, m_CurrentGizmoMode, NowCenterMatrix, NULL, NULL);

	//	フレーム中の移動量を計算
	ZVec3 nowMoveVec = NowCenterMatrix.GetPos() - old;




	auto UpdateMatrix = [this,nowMoveVec, NowCenterMatrix](ZSP<ECSEntity> ent) {
		auto mComp = ent->GetComponent<TransformComponent>();

		//	要素取得
		float matrixTranslation[3], matrixRotation[3], matrixScale[3];
		ImGuizmo::DecomposeMatrixToComponents(mComp->Transform, matrixTranslation, matrixRotation, matrixScale);

		ZVec3	VecFromCenter;
		VecFromCenter = mComp->Transform.GetPos() - NowCenterMatrix.GetPos();

		mComp->Transform.CreateIdentity();
		ZMatrix rotX, rotY, rotZ;
		rotX.CreateRotateX(matrixRotation[0]);
		rotY.CreateRotateY(matrixRotation[1]);
		rotZ.CreateRotateZ(matrixRotation[2]);
		mComp->Transform = rotX * rotY * rotZ;

		for (int i = 0; i < 3; i++) {
			if (fabsf(matrixScale[i]) < FLT_EPSILON) {
				matrixScale[i] = 0.001f;
			}
		}
		mComp->Transform.Scale_Local(matrixScale[0], matrixScale[1], matrixScale[2]);
		mComp->Transform.SetPos(VecFromCenter);
		mComp->Transform = mComp->Transform * NowCenterMatrix;

		mComp->Transform.Move(nowMoveVec);


		if (ent->GetComponent<ColliderComponent>()) {
			ent->GetComponent<ColliderComponent>()->UpdateTransform();
		}
	};

	//	追加選択を更新
	for (auto ent : SelectList) {
		UpdateMatrix(ent);
	}
	//	カレントを更新
	if (E_CurrentEntity->GetComponent<TransformComponent>()) {
		UpdateMatrix(E_CurrentEntity);
	}

	//	行列更新
	m_InBulkDat.BeforMatrix = NowCenterMatrix;
}

void LevelEditor::ShowModelNameList() {
	
	auto& Models = EditSystem.ModelList;

	for (auto& data :Models) {
		ImGui::Checkbox(data.first.c_str(), &data.second.addLoadModel);
	}

}

void LevelEditor::ShowModelNameCombo() {

	static ImGuiComboFlags flags = ImGuiComboFlags_None;

	if (ImGui::BeginCombo("SelectAddObjectModel", m_SelectModelName.c_str(), flags)) {

		bool is_selected = false;
		if (ImGui::Selectable(EmptyString.c_str(), is_selected)) {
			m_SelectModelName = EmptyString;
			ImGui::SetItemDefaultFocus();
		}

		for (auto& model : EditSystem.ModelList) {
			is_selected = (m_SelectModelName == model.first);

			if (ImGui::Selectable(model.first.c_str(), is_selected)) {
				m_SelectModelName = model.first;
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}

}

void LevelEditor::SelectObject() {

	auto hitObj = Make_Shared(Collider_Ray, appnew);
	// 基本設定
	hitObj->Init(0,
		HitGroups::_0, // 判定する側のフィルタ
		HitShapes::MESH,
		0
	);
	ZVec3 posA, posB, vec;
	posA	= EditSystem.GetCamera()->mCam.GetPos();
	ZVec3 mPos;
	mPos.Set(INPUT.GetMousePos().x, INPUT.GetMousePos().y,0.f);
	EditSystem.GetCamera()->Convert2Dto3D(posB, mPos);
	vec		= posB - posA;
	vec.Normalize();
	posB	= posB + (vec * 500.f);
	hitObj->Set(posA, posB);


	HitResData hitdat;
	hitdat.Init();
	for (auto& ent : EditSystem.GetEntities()) {
		if (ent->GetHandle() == NULL_ENTITY_HANDLE) { continue; }
		auto modelComp = ent->GetComponent<GameModelComponent>();
		if (!modelComp)continue;
		if (!modelComp->HitObj)continue;
		if (modelComp->HitObj->m_MeshTbl.empty())continue;


		if (HitTests::RayToMesh(*hitObj, &hitdat, *modelComp->HitObj, nullptr)) 
		{
		//	ZSP<EntityHandle> h = Make_Shared(EntityHandle, appnew);
		//	*h = ent->GetHandle();
			modelComp->HitObj->m_UserMap["Entity"] = ent;
			//			E_CurrentEntity = ent;//	とりあえずテスト
		}
	}

	float min = 500.f;
	for (auto& dat : hitdat.HitDataList) 
	{
		if (min > dat.RayDist) 
		{
			min = dat.RayDist;
			auto ent = reinterpret_cast<ECSEntity*>(dat.YouHitObj->m_UserMap.at("Entity").GetPtr());

			//	カレントが変わったら輪郭描画を切る
			if (E_CurrentEntity) 
			{
				auto model = E_CurrentEntity->GetComponent<GameModelComponent>();
				if (model) 
				{
					model->RenderFlg->DebugOutLine = false;
				}
			}
			//	カレント入れ替え
			E_CurrentEntity = ToZSP(ent);
		}
	}

	//	カレントが追加オブジェクトに選ばれてたらその選択を外す
	/*
	if (!E_AddSelectEntitys.empty())
	{
		int i = 0;
		for (auto ent : E_AddSelectEntitys) 
		{
			if (ent == E_CurrentEntity->GetHandle()) 
			{
				auto model = E_CurrentEntity->GetComponent<GameModelComponent>();
				if (model)
				{
					model->RenderFlg->DebugOutLine = false;
				}
				E_AddSelectEntitys.erase((E_AddSelectEntitys.begin() + i));
				break;
			}
		}
	}
	*/

	//	カレントが選ばれたら追加オブジェクトをすべて消す
	if (!E_AddSelectEntitys.empty()) 
	{
		for (auto ent : EditSystem.GetEntities())
		{
			for (auto add_ent : E_AddSelectEntitys) 
			{
				if (ent->GetHandle() == add_ent.Handle)
				{
					auto model = ent->GetComponent<GameModelComponent>();
					if (model) { model->RenderFlg->DebugOutLine = false; }
				}
			}
		}
		E_AddSelectEntitys.clear();
	}

}

void LevelEditor::CreateObjects_A(json11::Json & json) {

	m_SelectModelName = EmptyString;
	bool isCreate = false;
	for (auto& obj : json["Objects"].array_items()) {

		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		{
			auto comp = obj["GameModel"].object_items();
			auto model = APP.m_ResStg.LoadMesh(comp["ModelFileName"].string_value().c_str());
			modelcomp->Model = nullptr;
			if (model) {
				modelcomp->Model = model;
			}
			modelcomp->RenderFlg = Make_Shared(RenderState, appnew);

			RenderState flgs;
			flgs.Character			= comp["Character"].bool_value();
			flgs.BaseFrustumOut		= comp["BaseFrustumOut"].bool_value();
			flgs.NoneShadowRender	= comp["NoneShadowRender"].bool_value();
			flgs.SemiTransparent	= comp["SemiTransparent"].bool_value();
		}

		//	ローカル回転XYZ,ローカル拡大,座標設定の順に行う
		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();
		{
			ZVec3 p, r, s;
			auto comp = obj["Transform"].object_items();
			p.Set(comp["Position"]);
			r.Set(comp["Rotation"]);
			s.Set(comp["Scaling"]);

			ZMatrix& mat = transcomp->Transform;
			ZMatrix rotX, rotY, rotZ;

			rotX.CreateRotateX(r.x);
			rotY.CreateRotateY(r.y);
			rotZ.CreateRotateZ(r.z);

			mat = rotX * rotY * rotZ;

			//	一応エラーチェック
			for (int i = 0; i < 3; i++) {
				if (fabsf(s[i]) < FLT_EPSILON) {
					s[i] = 0.001f;
				}
			}
			mat.Scale_Local(s);
			mat.GetPos() = p;
		}

		auto entity = ECS.MakeEntity(transcomp, modelcomp);
		EditSystem.GetEntities().push_back(entity);


		//	カレントオブジェクトを更新
		E_CurrentEntity = entity;

		//	初期化モデルの名前を選択(無くてもいいけど一応)
		if (modelcomp->Model) {
			m_SelectModelName = modelcomp->Model->GetFullFileName();
		}
		isCreate = true;
	}

	if (!isCreate) {
		E_CurrentEntity = EditSystem.CreateEmptyModelEntity();
	}
}

void LevelEditor::CreateObjects_B(json11::Json & json) {
	
	m_SelectModelName = EmptyString;
	bool isCreate = false;
	for (auto& obj : json["Objects"].array_items()) {

		auto entity = CreateEntityFromJson(obj);
		EditSystem.GetEntities().push_back(entity);

		//	コライダーがある場合は更新
		if (entity->GetComponent<ColliderComponent>()) {
			entity->GetComponent<ColliderComponent>()->UpdateTransform();
		}

		//	カレントオブジェクトを更新
		E_CurrentEntity = entity;

		//	初期化モデルの名前を選択(無くてもいいけど一応)
		if (entity->GetComponent<GameModelComponent>()->Model) {
			m_SelectModelName = entity->GetComponent<GameModelComponent>()->Model->GetFullFileName();
		}
		isCreate = true;
	}

	if (!isCreate) {
		E_CurrentEntity = EditSystem.CreateEmptyModelEntity();
	}
}

void LevelEditor::ShowLevelMainPanel() {

	
	std::string imGuiWndName = "LevelEditor";

	if (ImGui::Begin(imGuiWndName.c_str()) == false) {
		ImGui::End();
		return;
	}

	//	Button
	if (ImGui::Button("SaveLevelObject")) {
		SelectSaveFile();
	}
	ImGui::SameLine();
	if (ImGui::Button("LoadLevelObject")) {
		if (!E_AddSelectEntitys.empty()) {
			ClearAddSelectObject();
		}
		SelectLoadFile();
	}

	//	Models
	if (ImGui::Button("LoadModel", ImVec2(ImGui::GetWindowContentRegionWidth()*0.5f, 20))) {
		LoadModel();
	}
	if (ImGui::CollapsingHeader("ShowModelList")) {
		ImGui::BeginChild("ModelNameList", ImVec2(ImGui::GetWindowContentRegionWidth(), 100), false, ImGuiWindowFlags_HorizontalScrollbar);
		{
			ShowModelNameList();
			ImGui::EndChild();
		}
	}

	//	Add & Remove
	if (ImGui::Button("AddObject", ImVec2(ImGui::GetWindowContentRegionWidth()*0.3f, 20))) {
		if (E_AddSelectEntitys.empty()) {
			AddObject();
		}
	}
	//ImGui::SameLine();
	ShowModelNameCombo();
	if (ImGui::Button("RemoveObject", ImVec2(ImGui::GetWindowContentRegionWidth()*0.3f, 20))) {
		if (E_AddSelectEntitys.empty()) {
			RemoveObject();
		}
	}

	//	DirLight
	ShMgr.m_LightMgr.ImGui_Add();



	ImGuiStyle* style = &ImGui::GetStyle();
	ImVec4*		colors = style->Colors;
	ImVec4		saveCol = colors[ImGuiCol_Button];

	colors[ImGuiCol_Button] = ImVec4(0.96f, 0.09f, 0.08f, 0.40f);

	if (ImGui::Button("ExportGameFile", ImVec2(ImGui::GetWindowContentRegionWidth()*0.5f, 30.f))) {
		SelectSaveFile(true);
	}

	colors[ImGuiCol_Button] = saveCol;


	ImGui::End();
}

void LevelEditor::ShowSelectEntity() {

	std::string imGuiWndName = "SelectEntityParams";

	if (!E_CurrentEntity) {
		return;
	}

	if (ImGui::Begin(imGuiWndName.c_str()) == false) {
		ImGui::End();
		return;
	}

	//	追加選択オブジェクトがあるときは表示なし
	if (!E_AddSelectEntitys.empty()) {
		EditTransform_InBulk(EditSystem.GetCamera()->mView, EditSystem.GetCamera()->mProj);
		ImGui::End();
		return;
	}

	if (E_CurrentEntity->GetComponent<TransformComponent>()) {
		EditTransform(
			EditSystem.GetCamera()->mView,
			EditSystem.GetCamera()->mProj,
			E_CurrentEntity->GetComponent<TransformComponent>()->Transform);
		//	行列更新
		if (E_CurrentEntity->GetComponent<ColliderComponent>()) {
			E_CurrentEntity->GetComponent<ColliderComponent>()->UpdateTransform();
		}
	}

	//	
	std::vector<__ECSComponentBase*> list;
	ECS.GetCompList(E_CurrentEntity->GetHandle(), list);
	ZString TName = "Transform";
	for (auto& comp : list) {
		if (comp->m_Name == TName) { continue; }	//	Transformは別で呼び出してるので無視
		string headerName = comp->m_Name.c_str();
//		headerName +="_" + to_string(E_CurrentEntity->GetHandle());
		if (ImGui::CollapsingHeader(headerName.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {

			auto tx = "Remove_" + headerName;
			if (ImGui::Button(tx.c_str())){
				//	レジスタで取得出来ない名前のコンポーネントはエラー落ちするので一応チェック
				if(comp->m_Name != "NonName"){
					ECS.RemoveComponent(E_CurrentEntity->GetHandle(), comp->m_Name);
				}
			}
			else {
				comp->ImGui();
			}
		}
	}

	//	Add
	AddComponentButton();


	ImGui::End();
}

void LevelEditor::ImGui() {

	auto ETransform = [this]
	{
		ShowSelectEntity();
	};
	DW_IMGUI_FUNC(ETransform);

	auto MainPanel = [this] {
		ShowLevelMainPanel();
	};
	DW_IMGUI_FUNC(MainPanel);

}

void LevelEditor::AddComponentButton() {

	static ZString selectCompName = "-----";
	static ImGuiComboFlags flags = ImGuiComboFlags_None;

	ZVector<ZString> compNames;
	ECS.GetComponentNames(compNames);

	ImGuiStyle* style		= &ImGui::GetStyle();
	ImVec4*		colors		= style->Colors;
	ImVec4		saveCol		= colors[ImGuiCol_Button];
	
	colors[ImGuiCol_Button] = ImVec4(0.96f, 0.09f, 0.08f, 0.40f);

	if (ImGui::Button("AddComponent", ImVec2(ImGui::GetWindowContentRegionWidth()*0.5f, 30.f))) {
		if (selectCompName != "-----") {
			AddComponent(selectCompName);
		}
	}

	colors[ImGuiCol_Button] = saveCol;
	
//	ImGui::SameLine();

	if (ImGui::BeginCombo("Components", selectCompName.c_str(), flags)) {

		bool is_selected = false;

		for (auto& name : compNames) {
			is_selected = (m_SelectModelName == name);

			if (ImGui::Selectable(name.c_str(), is_selected)) {
				selectCompName = name;
				ImGui::SetItemDefaultFocus();
			}
		}
		ImGui::EndCombo();
	}
}

void LevelEditor::AddComponent(ZString & compName) {
	if (E_CurrentEntity) {

		std::vector<__ECSComponentBase*> list;
		ECS.GetCompList(E_CurrentEntity->GetHandle(), list);

		for (auto& comp : list) {
			if (compName == comp->m_Name) { return; }	//	同名コンポーネントがすでにある場合は追加しない
		}

		auto comp = ECS.InstantiateComponent(compName);
		//comp.Instance->Init();

		//	一応編集できないものは追加しない
		if (comp.Instance->m_Name == "NonName") {
			return;
		}

		ECS.AddComponent(E_CurrentEntity, comp);
		comp.Instance->Init();	//	エンティティが必要になることもあるので追加後に初期化
	}
}

ZSP<ECSEntity> LevelEditor::CreateEntityFromJson(const json11::Json & jsonObj) {
	// エンティティ作成
	auto entity = ECS.MakeEntity();

	auto& compNames = jsonObj["CompList"].array_items();

	for (auto& name : compNames) {
		auto comp = ECS.InstantiateComponent(name.string_value().c_str());

		// jsonで初期化
		comp.Instance->InitFromJson(jsonObj[name.string_value()]);

		// コンポーネント追加
		ECS.AddComponent(entity, comp);
	}

	return std::move(entity);
}

//bool LevelEditor::AddAnimatorCompInit(InstanceData & inst) {/*
//	auto model	= E_CurrentEntity->GetComponent<GameModelComponent>();
//	auto bone	= E_CurrentEntity->GetComponent<ModelBoneControllerConponent>();
//	bool checkModel = false;
//	if (model) {
//		if (model->Model) { checkModel = true; }
//	}
//	if (bone) {
//		bone->BoneController->
//	}
//
//
//	return true;*/
//}

void LevelEditor::AddSelectObject() {
	auto hitObj = Make_Shared(Collider_Ray, appnew);
	// 基本設定
	hitObj->Init(0,
		HitGroups::_0, // 判定する側のフィルタ
		HitShapes::MESH,
		0
	);
	ZVec3 posA, posB, vec;
	posA	= EditSystem.GetCamera()->mCam.GetPos();
	ZVec3 mPos;
	mPos.Set(INPUT.GetMousePos().x, INPUT.GetMousePos().y,0.f);
	EditSystem.GetCamera()->Convert2Dto3D(posB, mPos);
	vec		= posB - posA;
	vec.Normalize();
	posB	= posB + (vec * 500.f);
	hitObj->Set(posA, posB);


	HitResData hitdat;
	hitdat.Init();
	for (auto& ent : EditSystem.GetEntities()) {
		if (ent->GetHandle() == NULL_ENTITY_HANDLE) { continue; }
		auto modelComp = ent->GetComponent<GameModelComponent>();
		if (!modelComp)continue;
		if (!modelComp->HitObj)continue;
		if (modelComp->HitObj->m_MeshTbl.empty())continue;


		if (HitTests::RayToMesh(*hitObj, &hitdat, *modelComp->HitObj, nullptr)) {
			modelComp->HitObj->m_UserMap["Entity"] = ent;
		}
	}

	float min = 500.f;
	ECSEntity*	ent = nullptr;
	for (auto& dat : hitdat.HitDataList) {
		if (min > dat.RayDist) {
			min = dat.RayDist;
			ent = reinterpret_cast<ECSEntity*>(dat.YouHitObj->m_UserMap.at("Entity").GetPtr());
		}
	}
	//	一番近いオブジェクトに対して処理
	if (ent) {
		//	カレントに無いオブジェクト(かつリストに含まれていない)エンティティだけを保存
		if (ent->GetHandle() != E_CurrentEntity->GetHandle()) {
			int check = -1;
			for (auto add_select : E_AddSelectEntitys) {
				if (add_select.Handle == ent->GetHandle()) {
					check = ent->GetHandle();
					continue;
				}
			}

			if (check == -1)	//	追加選択に含まれていないオブジェクトならリストに追加
			{
				AddSelectEntityData ADat;
				ADat.Handle = ent->GetHandle();
				ADat.SaveMatrix = ent->GetComponent<TransformComponent>()->Transform;
				E_AddSelectEntitys.push_back(ADat);
				
				m_InBulkDat.UpdateMatrix = true;
			}
			else {
				int i = 0;
				for (auto add_select : E_AddSelectEntitys) {
					if (add_select.Handle == check) {
						//auto model = E_CurrentEntity->GetComponent<GameModelComponent>();
						auto model = ent->GetComponent<GameModelComponent>();
						if (model) {
							model->RenderFlg->DebugOutLine = false;
						}
						E_AddSelectEntitys.erase((E_AddSelectEntitys.begin() + i));
						m_InBulkDat.UpdateMatrix = true;
					}
					i++;
				}
			}
		}
	}


}
void LevelEditor::ClearAddSelectObject() {

	if (!E_AddSelectEntitys.empty()) 
	{
		for (auto ent : EditSystem.GetEntities())
		{
			for (auto add_ent : E_AddSelectEntitys) 
			{
				if (ent->GetHandle() == add_ent.Handle)
				{
					auto model = ent->GetComponent<GameModelComponent>();
					if (model) { model->RenderFlg->DebugOutLine = false; }
				}
			}
		}
		E_AddSelectEntitys.clear();
	}

}

void LevelEditor::CopyObjects() {

}

void LevelEditor::PasteObjects() {

}

void LevelEditor::ClearClipObjects() {
	//m_ClipData.clear();
}