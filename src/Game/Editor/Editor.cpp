#include "MainFrame/ZMainFrame.h"
#include "Game/System/CommonECSComponents/CommonComponents.h"
#include "Game/System/TestECSConponents/TestComponents.h"
#include "Game/System/TestECSSystems/TestSystems.h"
#include "Game/System/MapObject/MapObjectComponents/MapObjectComponents.h"
#include "Editor.h"
#include "Game/Camera/GameCamera.h"

Editor::~Editor() {
	Release();
}

void Editor::Init() {
	
	Release();

	ECS.RegisterClassRefrection<GameModelComponent>("GameModel");
	ECS.RegisterClassRefrection<AnimatorComponent>("Animator");
	ECS.RegisterClassRefrection<TransformComponent>("Transform");
	ECS.RegisterClassRefrection<ColliderComponent>("Collider");
	ECS.RegisterClassRefrection<ModelBoneControllerConponent>("ModelBoneController");
	ECS.RegisterClassRefrection<SecurityComponent>("Security");
	ECS.RegisterClassRefrection<ServerComponent>("Server");

	m_UseEditor = UseEditorFlgs::Level;
	m_PanelFlg.AllFalse();
#if _DEBUG
	m_PanelFlg.PostEffect = true;
#endif

	//	PhysicsCreate
	{
		m_PhysicsWorld = Make_Shared(ZPhysicsWorld,appnew);
		m_PhysicsWorld->Init();
	}
	//	ShadersParam
	{

		m_DirLight = ShMgr.m_LightMgr.GetDirLight();
		m_DirLight->SetData
		(
			ZVec3(0.8f, -1, 0.8f),		// ライト方向
			ZVec4(0.7f, 0.7f, 0.7f, 1)	// 基本色
		);

		// 環境色
		ShMgr.m_LightMgr.m_AmbientLight.Set(0.3f, 0.3f, 0.3f);
		//	Blur Init
		ShMgr.m_Blur.CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));

	}
	//	CameraInit
	{
		m_Cam = Make_Shared(GameCamera,appnew);
		// カメラ初期化
		m_Cam->Init(0, 0, -3);
		m_Cam->m_BaseMat.SetPos(0, 1.2f, 0);
		
	}
	//	PostEffectCreate
	{
		m_PostEffect.Init();
		m_PostEffect.LoadState("data/Scene/EditScene/PostState.json");		//	ポストエフェクトステート入力
		//m_PostEffect.SaveState("data/Scene/EditScene/PostState.json");	//	ポストエフェクトステート出力
	}

	//	Sky Texture
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");

	m_LE.Init();

}

void Editor::Release() {

	m_Cam		= nullptr;
	m_DirLight	= nullptr;
	m_PostEffect.Release();
	
	if (m_AddSelectEntitys.size()) {
		m_AddSelectEntitys.clear();
	}
	m_CurrentEntity = nullptr;

	//	レベルエディタ解放
	m_LE.Release();

	//	モデル解放
	ModelList.clear();

	DeleteAllEntity();

	if(m_PhysicsWorld)m_PhysicsWorld->Release();
}

void Editor::Update() {

	//カメラ操作
	if (m_Cam) {
		m_Cam->Update();
	}


	switch (m_UseEditor) {
	case Level:
		m_LE.Update();
		break;
	default:
		break;
	}


	if (m_CurrentEntity) {
		auto model = m_CurrentEntity->GetComponent<GameModelComponent>();
		if (model) {
			model->RenderFlg->DebugOutLine = true;
			model->RenderFlg->OutLineCol = ZVec4(1.f, 0.3f, 0, 1.f);
		}
	}
	if (!m_AddSelectEntitys.empty()) {
		for (auto& ent : m_AddSelectEntitys) {
			for (auto obj : m_Entities) {
				if (obj->GetHandle() == ent.Handle) {
					auto model = obj->GetComponent<GameModelComponent>();
					if (model) {
						model->RenderFlg->DebugOutLine = true;
						model->RenderFlg->OutLineCol = ZVec4(1.f, 0.3f, 0, 1.f);
					}
				}
			}
	
		}
	}
}

void Editor::Draw() {
	//	シャドウマップ描画	
	{
		ShMgr.SetShadowCamTargetPoint(m_Cam->mCam.GetPos());	//	現在のカメラをセットEditRenderFlgs
		Renderer.DrawShadow();
	}

	//	3D描画
	ShMgr.m_Ls.Flash();

	if (m_RenderFlgs.ShowModels) {
		//	モデル描画
		ShMgr.m_Ms.Draw();
	}
	else {

		ShMgr.m_Ms.m_SkinMeshRenderer->BufferClear();
		ShMgr.m_Ms.m_StaticModelRender->BufferClear();
		ShMgr.m_Ms.m_InstSMeshRenderer->BufferClear();
	}


	if (m_RenderFlgs.ShowCollisions) {
		//	コリジョン描画
		ShMgr.m_Ms.DrawCollider();
		ShMgr.m_Ls.Flash();
	}
	else {
		ShMgr.m_Ms.m_CollisionRender->BufferClear();
		ShMgr.m_Ls.BufferClear();
	}

}

void Editor::DrawSky() {
	ShMgr.m_Ss.Begin(false, true);
	ShMgr.m_Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, 1280, 720);
	ShMgr.m_Ss.End();
}

void Editor::ImGui() {
	
	//	メインパネル
	EditorMainPanel();

	if (m_PanelFlg.ModelRender)Renderer.ImGui();
	if (m_PanelFlg.PostEffect)m_PostEffect.ImGui();
	if (m_PanelFlg.Blur)ShMgr.m_Blur.ImGui();

	switch (m_UseEditor) {
	case Level:
		m_LE.ImGui();
		break;
	default:
		break;
	}
}

ZSP<ECSEntity>& Editor::CreateEmptyModelEntity() {

	GameModelComponent* modelcomp	= ECS.MakeComponent<GameModelComponent>();
	modelcomp->Model				= nullptr;
	modelcomp->RenderFlg			= Make_Shared(RenderState,appnew);
	modelcomp->RenderFlg->Character = false;

	TransformComponent* transcomp	= ECS.MakeComponent<TransformComponent>();
	auto entity						= ECS.MakeEntity(transcomp, modelcomp);
	m_Entities.push_back(entity);

	//	今作ったエンティティを返す
	return m_Entities.back();
}

void Editor::SaveInitFile() {

}

void Editor::LoadInitFile() {

}

void Editor::EditorMainPanel() {

	auto EditorMain = [this]
	{
		std::string imGuiWndName = "Editor";
		if (ImGui::Begin(imGuiWndName.c_str()) == false) {
			ImGui::End();
			return;
		}

		//	使用エディタセレクタ
		{
		/*	ImGui::BeginChild("UseEditor", ImVec2(ImGui::GetWindowContentRegionWidth(), 200), false, ImGuiWindowFlags_HorizontalScrollbar);

			ImGui::EndChild();*/
		}

		//	存在するコンポーネント
		{
			EntitySelecter();
		}
		//	描画切り換え
		{
			if (ImGui::CollapsingHeader("DebugRenderMode")) {
				if (ImGui::Button("ModelOnly")) {
					m_RenderFlgs.AllFalse();
					m_RenderFlgs.ShowModels = true;
				}
				ImGui::SameLine();
				if (ImGui::Button("CollisionOnly")) {
					m_RenderFlgs.AllFalse();
					m_RenderFlgs.ShowCollisions = true;
				}

				ImGui::Checkbox("ShowModels", &m_RenderFlgs.ShowModels);
				ImGui::Checkbox("ShowCollisions", &m_RenderFlgs.ShowCollisions);
			}
		}
		//	デバッグ描画用
		{
			if (ImGui::CollapsingHeader("DebugFlgs")) {
				ImGui::BeginChild("DebugPanels", ImVec2(ImGui::GetWindowContentRegionWidth(), 50), false, ImGuiWindowFlags_HorizontalScrollbar);

				ImGui::Checkbox("ModelRender", &m_PanelFlg.ModelRender);
				ImGui::Checkbox("PostEffect", &m_PanelFlg.PostEffect);
				ImGui::Checkbox("Blur", &m_PanelFlg.Blur);

				ImGui::EndChild();
			}
		}
		//	追加選択オブジェクト一覧
		{
			if (ImGui::CollapsingHeader("AddSelectObject")) {
				for (auto obj : m_AddSelectEntitys) {
					string tx = std::to_string(obj.Handle);
					ImGui::Text(tx.c_str());
				}
			}
		}
		

		ImGui::End();
	};

	DW_IMGUI_FUNC(EditorMain);
}

void Editor::EntitySelecter() {
	
	static size_t selectEnt = 0;

	if (ImGui::BeginChild("Objects", ImVec2(ImGui::GetWindowContentRegionWidth(), 200), false, ImGuiWindowFlags_HorizontalScrollbar)) 
	{
		for (auto& ent : m_Entities) 
		{

			if (ent->GetHandle() == NULL_ENTITY_HANDLE) { continue; }

			bool flg = false;
			if (selectEnt == ent->GetHandle()) { flg = true; }

			string tx = std::to_string(ent->GetHandle());
			if (ImGui::RadioButton(tx.c_str(), flg)) {
				selectEnt = ent->GetHandle();

				//	カレントが変わったら輪郭描画を切る
				auto model = m_CurrentEntity->GetComponent<GameModelComponent>();
				if (model) {
					model->RenderFlg->DebugOutLine = false;
				}

				E_CurrentEntity = ent;
			}

		}

		selectEnt = E_CurrentEntity->GetHandle();

		ImGui::EndChild();
	}
}