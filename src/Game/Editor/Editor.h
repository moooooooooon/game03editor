#pragma once

class GameCamera;

struct PanelFlgs {
	bool ModelRender	= true;
	bool PostEffect		= true;
	bool Blur			= true;
	bool StaticModelRenderer = true;
public:
	void AllTrue() {
		ModelRender = true;
		PostEffect	= true;
		Blur		= true;
		StaticModelRenderer = true;
	}
	void AllFalse() {
		ModelRender = false;
		PostEffect	= false;
		Blur		= false;
		StaticModelRenderer = false;
	}

	template<class Archive>
	void serialize(Archive & archive) {
		archive(cereal::make_nvp("ModelRender",ModelRender));
		archive(cereal::make_nvp("PostEffect", PostEffect));
		archive(cereal::make_nvp("Blur", Blur));
		archive(cereal::make_nvp("LightManager", LightManager));
	}
};

enum UseEditorFlgs{
	Level	=	0,
};

//	エディタのデバッグ描画フラグ
struct EditRenderFlgs {
	bool ShowModels		= true;		//	通常モデル描画を行う
	bool ShowCollisions = false;	//	当たり判定モデルを描画する

	void AllFalse() {
		ShowModels		= false;
		ShowCollisions	= false;
	}

	void AllTrue() {
		ShowModels		= true;
		ShowCollisions	= true;
	}
};

//	エディタ用のモデルデータ(追加読み込みフラグ等)
struct EditorModelData {
	ZSP<ZGameModel>	model;
	bool			addLoadModel;
};

//	追加選択のオブジェクトのデータ
struct AddSelectEntityData {
	AddSelectEntityData() {
		//TmpMatrix = ZMatrix::Identity;
	}
	EntityHandle		Handle;
//	ZMatrix				TmpMatrix;
	ZMatrix				SaveMatrix;	//	リストに追加されたときの行列保存用
};

class Editor {

public:
	~Editor();

	void				Init();
	void				Release();
	void				Update();
	void				Draw();
	void				DrawSky();
	void				ImGui();

	void				DeleteAllEntity() {
		ECSEntity::RemoveAllEntity(m_Entities);
		m_Entities.shrink_to_fit();
	}
	ZSP<ECSEntity>&	CreateEmptyModelEntity();

	//	accessor
	PostEffect&			GetPostEffect() { return m_PostEffect; }
	ZSP<GameCamera>		GetCamera()		{ return m_Cam; }
	auto&				GetEntities()	{ return m_Entities; }
	auto&				GetCurrentEntity() { return m_CurrentEntity; }
	auto&				GetAddSelectEntitys() { return m_AddSelectEntitys; }
public:
	map<ZString, EditorModelData>	ModelList;

private:
	void SaveInitFile();
	void LoadInitFile();

	void EditorMainPanel();
	void EntitySelecter();

private:
	ZAVector<ZSP<ECSEntity>> m_Entities;
	ZSP<DirLight>			m_DirLight;
	ZSP<ZPhysicsWorld>		m_PhysicsWorld;
	ZSP<ZTexture>			m_SkyTex;
	ZSP<GameCamera>			m_Cam;
	PostEffect				m_PostEffect;

	ZSP<ECSEntity>			m_CurrentEntity;					//　選択しているオブジェクト


	vector<AddSelectEntityData>	m_AddSelectEntitys;					//	複数選択よう(本当は上記を配列にしていいんだけど、修正箇所が多いのでこっちで)

	
	//---------------------------------------------------------------------------
	//	EditState
	//---------------------------------------------------------------------------

	PanelFlgs				m_PanelFlg;
	UseEditorFlgs			m_UseEditor;

	LevelEditor				m_LE;

	EditRenderFlgs			m_RenderFlgs;

public:
	/*------------------------------------------------*/
	//	シングルトン
	/*------------------------------------------------*/
	static Editor& GetInstance(void) {
		static Editor Instance;
		return Instance;
	}

private:
	// コンストラクタ
	Editor(void) = default;
	// コピー禁止用
	Editor(const Editor& _src) {}
	void operator=(const Editor& _src) {}

};

#define EditSystem	Editor::GetInstance()
#define E_CurrentEntity EditSystem.GetCurrentEntity()
#define E_AddSelectEntitys EditSystem.GetAddSelectEntitys()