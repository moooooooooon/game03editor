#include "ZMMDMaterial.h"

namespace EzLib
{
	ZMMDMaterial::ZMMDMaterial()
		: Diffuse(1),
			Alpha(1),
			Specular(0),
			SpePow(1),
			Ambient(0.2f),
			EdgeFlag(0),
			EdgeSize(0),
			EdgeColor(0.0f),
			TextureMulFactor(1),
			SpTextureMulFactor(1),
			ToonTextureMulFactor(1),
			TextureAddFactor(0),
			SpTextureAddFactor(0),
			ToonTextureAddFactor(0),
			GroundShadow(true),
			ShadowCaster(true),
			ShadowReceiver(true)
	{
	}

}
