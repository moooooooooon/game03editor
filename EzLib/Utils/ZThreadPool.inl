inline size_t ZThreadPool::GetNumThreads() const
{
	return m_Threads.size();
}

template<class T>
inline auto ZThreadPool::AddTask(T runnable)->std::future<decltype(runnable())>
{
	ZSP<std::packaged_task<decltype(runnable())()>> wrapper = 
				Make_Shared(std::packaged_task<decltype(runnable())()>, sysnew, std::move(runnable));
	
	{
		std::unique_lock<std::mutex> ul(m_Mtx);
		m_Tasks.push([=] { (*wrapper)(); });
	}

	// スレッド起動
	m_CV.notify_one();

	return wrapper->get_future();
}