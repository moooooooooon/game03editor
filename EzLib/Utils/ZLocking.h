#ifndef ZLocking_h
#define ZLocking_h

namespace EzLib
{

	//===============================================================
	//  @file ZLocking.h
	//   スレッドの排他制御などをおこなう機能
	// 
	//  @author 鎌田
	//===============================================================




	//========================================
	//  マルチスレッド排他制御
	// クリティカルセクションでの排他制御
	// 
	//  @ingroup Etc
	//========================================
	class ZLock
	{
	public:
		// ロック開始
		void Lock()
		{
			EnterCriticalSection(&m_Lock);
		}
		// ロック解除
		void Unlock()
		{
			LeaveCriticalSection(&m_Lock);
		}

		ZLock()
		{
			InitializeCriticalSection(&m_Lock);
		}
		~ZLock()
		{
			DeleteCriticalSection(&m_Lock);
		}

	private:
		// クリティカルセクションでロックする
		CRITICAL_SECTION	m_Lock;
	};

	//========================================
	//   排他制御なし
	// 
	//  @ingroup Etc
	//========================================
	class ZNoLock
	{
	public:
		// ロック開始
		void Lock()
		{
		}
		// ロック解除
		void Unlock()
		{
		}
	};


	//========================================
	//   排他制御クラスを使いやすくするクラス
	// 
	//  @ingroup Etc
	//========================================
	template<typename LockType>
	class ZLockGuard
	{
	public:
		ZLockGuard(LockType& lockObj) : m_refLock(lockObj)
		{
			lockObj.Lock();
		}
		~ZLockGuard()
		{
			m_refLock.Unlock();
		}

	private:
		LockType& m_refLock;
	};


}

#endif
