#include "EzLib.h"

using namespace EzLib;

// 日本語判定
//#define isSJIS(a) ((BYTE)a >= 0x81 && (BYTE)a <= 0x9f || (BYTE)a >= 0xe0 && (BYTE)a<=0xfc)

// 日本語判定
static bool isSJIS(char a)
{
	return ((BYTE)a >= 0x81 && (BYTE)a <= 0x9f || (BYTE)a >= 0xe0 && (BYTE)a <= 0xfc);
}

bool ZCsv::Load(const ZString& filename, bool bDelTitle)
{
	m_Tbl.clear();

	FILE* fp;
	fopen_s(&fp,filename.c_str(), "rb");
	if (fp == nullptr)return false;

	fseek(fp, 0L, SEEK_END);	//あえて最初からファイルポインタの位置を最後にする
	int filesize = ftell(fp);	//ファイルサイズを代入
	fseek(fp, 0L, SEEK_SET);    //最初に戻して次の処理に備える
	char* buf = new char[filesize + 1];
	fread(buf, filesize, 1, fp);
	fclose(fp);

	buf[filesize] = '\0';

	ZString ttt = buf;
	bool bRet = LoadFromText(buf, bDelTitle);

	delete buf;
	return bRet;

}

static bool IsKAIGYO(const char* str)
{
	if (*str == '\n')
	{
		return true;
	}
	else if (strncmp(str, "\r\n", 2) == 0)
	{
		return true;
	}
	return false;
}

bool ZCsv::LoadFromText(const ZString& text, bool bDelTitle)
{
	m_Tbl.clear();

	if (text.size() == 0)return false;

	ZVector<ZString> tmp;


	// 現在読み込み中の文字列内の位置
	const char* pNow = text.c_str();

	// 1セルを読み込む
	//  戻り値 : 0 … 次もデータがある
	//           1 … この行は終わり
	//           2 … 終端
	auto readCel = [&pNow](ZString& outStr)
	{
		const char* pRead = pNow;

		//----------------------------
		// 「"」指定がある時は、次の「"」まで一気に読み込む
		//----------------------------
		if (*pRead == '"' && strncmp(pRead, "\"\"", 2) != 0)
		{
			pRead++;
			const char* pEnd = pRead;
			while (1)
			{
				// 日本語文字
				if (isSJIS(*pEnd))
				{
					pEnd += 2;
					continue;
				}

				// 次の「"」か？
				if (*pEnd == '"' && strncmp(pEnd, "\"\"", 2) != 0)
				{
					// 文字列コピー
					int n = pEnd - pRead;
					outStr.resize(n);
					strncpy_s(&outStr[0], n, pRead, n);


					// 次の文字の位置へ更新
					pNow = pEnd + 1;

					// 「,」
					if (*pNow == ',')
					{
						pNow += 1;
						return 0;	// 次もデータあり
					}
					else if (*pNow == '\n')
					{
						pNow += 1;
						return 1;	// この行は終わり
					}
					else if (strncmp(pNow, "\r\n", 2) == 0)
					{
						pNow += 2;
						return 1;	// この行は終わり
					}
					else
					{
						return 999;	// 次は終端
					}


				}
				pEnd++;
			}
		}
		//----------------------------
		// 通常読み込み
		//----------------------------
		else
		{
			const char* pEnd = pRead;
			while (1)
			{
				// 日本語文字
				if (isSJIS(*pEnd))
				{
					pEnd += 2;
					continue;
				}

				// 区切り文字発見
				if (*pEnd == ',' || IsKAIGYO(pEnd) || *pEnd == '\0')
				{
					int n = pEnd - pRead;
					// 0文字
					if (n == 0)
					{
						outStr = "";
					}
					else
					{
						outStr.resize(n);
						strncpy_s(&outStr[0], n, pRead, n);
					}

					// 位置更新
					pNow = pEnd;

					// 「,」
					if (*pNow == ',')
					{
						pNow += 1;
						return 0;	// 次もデータあり
					}
					else if (*pNow == '\n')
					{
						pNow += 1;
						return 1;	// この行は終わり
					}
					else if (strncmp(pNow, "\r\n", 2) == 0)
					{
						pNow += 2;
						return 1;	// この行は終わり
					}
					else
					{
						return 999;	// 次は終端
					}
				}

				pEnd++;

			}
		}

	};


	// 行ループ
	while (1)
	{
		// 終端
		if (*pNow == '\0')break;

		ZString cel;
		while (1)
		{
			// 1セル読み込み
			int ret = readCel(cel);

			// データ追加
			tmp.push_back(cel);

			// この行は終わり
			if (ret == 1)
			{
				break;
			}

			// 終端
			if (ret == 999)
			{
				break;
			}


		}

		// 本データへ追加
		m_Tbl.push_back(tmp);
		// tmpクリア
		tmp.clear();
	}

	// 1行目を消す
	if (bDelTitle)
	{
		if (m_Tbl.size() > 0)
		{
			m_Tbl.erase(m_Tbl.begin());
		}
	}

	/*
	ZString str;
	const char* s = text.c_str();
	bool bAdd=true;
	while(1){
		// 終端
		if(*s == '\0'){
			break;
		}
		tmp.clear();



		const char* pRead = s;
		while(1){
			// 改行なら次へ
			if(*s == '\n'){
				s++;
				break;
			}
			else if(strncmp(s, "\r\n", 2) == 0){
				s+=2;
				break;
			}
			if (*pRead == ',') {
//				tmp.push_back("");
//				pRead++;
//				continue;
			}

			if(*pRead == '\0'){
				s = pRead;
				break;
			}

			if(*pRead == '"' && strncmp(pRead,"\"\"",2) != 0){	// 文字列指定がある！
				pRead++;
				const char* pEnd = pRead;
				while(1){
					if(isSJIS(*pEnd)){
						pEnd++;
					}
					else{
						if(*pEnd == '"' && strncmp(pEnd,"\"\"",2) != 0){	// 文字列指定がある！
							int n = pEnd - pRead;
							str.resize(n);
							strncpy(&str[0],pRead,n);
//							str[n] = '\0';
							tmp.push_back(str);
							pRead = pEnd;

							pRead++;	// "

							if (*pRead != '\0') {
								pRead++;	// ,
							}

							s = pRead;

							break;
						}
						pEnd++;
					}
				}
			}
			else{
				const char* pEnd = pRead;
				while(1){
					if(isSJIS(*pEnd)){
						pEnd++;
					}
					else{
						if(*pEnd == ',' || IsKAIGYO(pEnd) || *pEnd == '\0'){	// 区切り
							int n = pEnd - pRead;
							if (n == 0) {
								tmp.push_back("");
							}
							else {
								str.resize(n);
								strncpy(&str[0], pRead, n);
								//							str[n] = '\0';
								tmp.push_back(str);
							}
							pRead = pEnd;

							pRead++;
//							if(*pRead != '\0'){
//								pRead++;	// ,
//							}

							s = pRead;

							break;
						}

						pEnd++;
					}
				}
			}
		}

		if(bDelTitle){
			bDelTitle = false;
		}
		else{
			// 行に追加
			if(bAdd){
				m_Tbl.push_back(tmp);
				tmp.clear();
			}
		}
	}
	*/

	/*
	// Check
	UINT maxCol = 0;
	for (auto& row : m_Tbl) {
		if (maxCol < row.size())maxCol = row.size();
	}

	for (auto& row : m_Tbl) {
		for (int i = 0; i < maxCol - (int)row.size(); i++) {
			row.push_back("");
		}
	}
	*/

	return true;
}

//  文字列を置換する
static ZString Replace(ZString String1, ZString String2, ZString String3)
{
	ZString::size_type  Pos(String1.find(String2));

	while (Pos != ZString::npos)
	{
		String1.replace(Pos, String2.length(), String3);
		Pos = String1.find(String2, Pos + String3.length());
	}

	return String1;
}

bool ZCsv::Save(const ZString& filename) const
{
	std::ofstream ofs(filename.c_str(), std::ios_base::binary);
	if (!ofs)
	{
		return false;
	}

	for (UINT y = 0; y < m_Tbl.size(); y++)
	{
		for (UINT x = 0; x < m_Tbl[y].size(); x++)
		{
			bool bStr = false;
			// ","文字判定
			if (ZString::npos != m_Tbl[y][x].find(","))
			{
				bStr = true;
			}
			// 改行判定
			if (ZString::npos != m_Tbl[y][x].find("\n"))
			{
				bStr = true;
			}
			// 日本語判定
			/*
			for(UINT si=0;si<inData[y][x].size();si++){
				if(isSJIS(inData[y][x][si])){
					bStr = true;
					break;
				}
			}
			*/

			if (bStr)
			{
				ofs << "\"";
			}

			ZString sTmp = m_Tbl[y][x];
			if (bStr)
			{
				sTmp = Replace(sTmp, "\"", "\"\"");
			}
			ofs << sTmp;

			if (bStr)
			{
				ofs << "\"";
			}

			if (x < m_Tbl[y].size() - 1)
			{
				ofs << ",";
			}
		}
		ofs << std::endl;
	}

	return true;
}
