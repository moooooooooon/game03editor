template<typename T>
inline ZObjectForTree<T>::ZObjectForTree()
	: m_pSpace(nullptr),
	m_pObject(nullptr),
	m_pPrev(nullptr),
	m_pNext(nullptr)
{
}

template<typename T>
inline ZObjectForTree<T>::~ZObjectForTree()
{
}

template<typename T>
inline bool ZObjectForTree<T>::Remove()
{
	if (m_pCell == nullptr)
		return false;

	// 空間に通知
	m_pCell->OnRemove(this)

	// このオブジェクトの前後のオブジェクトの結びつけ
	if (m_pPrev != nullptr)
	{
		m_pPrev->m_pNext = m_pNext;
		m_pPrev = nullptr;
	}

	if (m_pNext != nullptr)
	{
		m_pNext->m_pPrev = m_pPrev;
		m_pNext = nullptr;
	}

	m_pCell = nullptr;
	return true;
}

template<typename T>
inline ZSP<ZObjectForTree<T>>& ZObjectForTree<T>::GetNextObj()
{
	return m_pNext;
}

template<typename T>
inline void ZObjectForTree<T>::RegisterSpace(ZOctreeSpace<T>* pSpace)
{
	m_pSpace = pSpace;
}

template<typename T>
inline ZOctree<T>::ZOctree()
			: m_pSpaceArray(nullptr),
			  m_RgnWidth(1),
			  m_RgnMin(0),
			  m_RgnMax(1),
			  m_Unit(1),
			  m_NumSpace(0),
			  m_LowestLevel(0)
{
	// 各レベルの空間数算出
	m_NumOfSpaceByLevel[0] = 1;
	for (int i = 1; i < MaxSpaceLevel + 1; i++)
		m_NumOfSpaceByLevel[i] = m_NumOfSpaceByLevel[i - 1] * 8;
}

template<typename T>
inline ZOctree<T>::~ZOctree()
{
	Clear();
	SAFE_DELPTR(m_pSpaceArray);
}

template<typename T>
inline bool ZOctree<T>::Init(uint splitLevel, const ZAABB& octreeArea)
{
	if (splitLevel >= MaxSpaceLevel)
		return false;

	// レベル(0基点)の配列作成
	m_NumSpace = (m_NumOfSpaceByLevel[splitLevel + 1] - 1) / 7;
	m_pSpaceArray = sysnewArray(ZOctreeSpace<T>*,m_NumSpace);
	memset(m_pSpaceArray, 0, sizeof(ZOctreeSpace<T>*) * m_NumSpace);

	// 領域を登録
	m_RgnMin = octreeArea.Min;
	m_RgnMax = octreeArea.Max;
	m_RgnWidth = octreeArea.Max - octreeArea.Min;
	m_Unit = m_RgnWidth / (float)(1 << splitLevel);

	m_LowestLevel = splitLevel;

	return true;
}

template<typename T>
inline void ZOctree<T>::Clear()
{
	for (uint i = 0; i < m_NumSpace; i++)
	{
		if (m_pSpaceArray[i] != nullptr)
			SAFE_DELPTR(m_pSpaceArray[i]);
	}
}

template<typename T>
inline void ZOctree<T>::CreateNewSpace(uint elem)
{
	// elem番号の空間が作られていなければ
	while (m_pSpaceArray[elem] == nullptr)
	{
		// 指定の要素番号に空間を作成
		m_pSpaceArray[elem] = sysnew(ZOctreeSpace<T>);

		// 親空間に移動
		// 親空間も作られていなければ空間作成
		elem = (elem - 1) >> 3;
		if (elem >= m_NumSpace)
			break;
	}
}


template<typename T>
inline bool ZOctree<T>::RegisterObject(const ZAABB& aabb, T* pObj)
{
	ZSP<ZObjectForTree<T>> pOFT = Make_Shared(ZObjectForTree<T>, sysnew);
	pOFT->m_pObject = pObj;
	return RegisterObject(aabb, pOFT);
}

template<typename T>
inline bool ZOctree<T>::RegisterObject(const ZAABB& aabb, ZSP<ZObjectForTree<T>>& pOFT)
{
	uint elem = GetMortonNumber(aabb);
	if (elem >= m_NumSpace)
		return false;

	if (m_pSpaceArray[elem] == nullptr)
		CreateNewSpace(elem);

	return m_pSpaceArray[elem]->Push(pOFT);
}

template<typename T>
inline size_t ZOctree<T>::GetAllCollisionList(ZVector<T*>& colList)
{
	// リスト初期化
	colList.clear();

	// ルート空間の存在チェック
	if (m_pSpaceArray[0] == nullptr)
		return 0; // 空間が存在しない

	// ルート空間を処理
	ZList<T*> colStac;
	GetCollisionList(0, colList, colStac);

	return colList.size();
}

template<typename T>
inline size_t ZOctree<T>::GetCollisionList(const ZAABB& aabb, ZVector<T*>& colList)
{
	size_t elem = GetMortonNumber(aabb);

	colList.clear();
	if (m_pSpaceArray[elem] == nullptr)
		return 0; // 空間が存在しない

	// 空間内のオブジェクト同士の衝突リスト作成
	uint numObj = 0;
	uint nextElem = elem;
	for (uint i = 0; i < 8; i++)
	{
		if (nextElem >= m_NumSpace && m_pSpaceArray[nextElem])
			break;
		
		if (m_pSpaceArray[nextElem] == nullptr)
			break;

		ZSP<ZObjectForTree<T>> pOFT = m_pSpaceArray[nextElem]->GetFirstObj();
		while (pOFT != nullptr)
		{
			colList.push_back(pOFT->m_pObject);
			pOFT = pOFT->m_pNext;
		}
		
		nextElem = elem * 8 + 1 + i;
	}

	return colList.size();
}

template<typename T>
inline void ZOctree<T>::GetCollisionList(uint elem, ZVector<T*>& colList, ZList<T*>&colStac)
{
	// 空間内のオブジェクト同士の衝突リスト作成
	ZSP<ZObjectForTree> pOFT1 = m_pSpaceArray[elem]->GetFirstObj();
	while (pOFT1 != nullptr)
	{
		ZSP<ZObjectForTree> pOFT2 = pOFT1->pNext;
		while (pOFT2 != nullptr)
		{
			// 衝突リスト作成
			colList.push_back(pOFT1->m_pObject);
			colList.push_back(pOFT2->m_pObject);
			pOFT2 = pOFT2->m_pNext;
		}

		// 衝突スタックとの衝突リスト作成
		for (auto it = colStac.begin(); it != colStac.end(); it++)
		{
			colList.push_back(pOFT1->m_pObject);
			colList.push_back(*it);
		}

		pOFT1 = pOFT1->m_pNext;
	}

	bool childFlg = false;
	// 子空間に移動
	uint numObj = 0;
	uint nextElem;
	for (uint i = 0; i < 8; i++)
	{
		nextElem = elem * 8 + 1 + i;
		if (nextElem < m_NumSpace && m_pSpaceArray[nextElem])
		{
			if (childFlg == false)
			{
				// 登録オブジェクトをスタックに追加
				pOFT1 = m_pSpaceArray[elem]->GetFirstObj();
				while (pOFT1 != nullptr)
				{
					colStac.push_back(pOFT1->m_pObject);
					numObj++;
					pOFT1 = pOFT1->m_pNext;
				}
			}
			childFlg = true;
			GetCollisionList(nextElem, colList, colStac); // 小空間へ
		}
	}

	if (childFlg == false)
		return;

	// スタックからオブジェクトを外す
	for (uint i = 0; i < numObj; i++)
		colStac.pop_back();
}

template<typename T>
inline uint ZOctree<T>::GetMortonNumber(const ZAABB& aabb)const
{
	// 最小レベルにおける各軸位置を算出
	uint8 LT = GetPointElem(aabb.Min);
	uint8 RB = GetPointElem(aabb.Max);

	// 空間番号を引き算し、
	// 最上位区切りから所属レベルを算出
	uint8 def = RB ^ LT;
	uint hiLevel = 1;
	for (uint i = 0; i < m_LowestLevel; i++)
	{
		uint chack = (def >> (i * 3)) & 0x7;
		if (chack != 0)
			hiLevel = i + 1;
	}

	uint spaceNum = RB >> (hiLevel * 3);
	uint addNum = (m_NumOfSpaceByLevel[m_LowestLevel - hiLevel] - 1) / 7;
	spaceNum += addNum;

	if (spaceNum > m_NumSpace)
		return 0xFFFFFFFF;

	return spaceNum;
}

template<typename T>
inline uint ZOctree<T>::Get3DMortonNumber(uint8 x, uint8 y, uint8 z)const
{
	return BitSeparate(x) | BitSeparate(y) << 1 | BitSeparate(z) << 2;
}

template<typename T>
inline uint ZOctree<T>::BitSeparate(uint8 n)const
{
	uint s = n;
	s = (s | s << 8) & 0x0000F00F;
	s = (s | s << 4) & 0x000C30C3;
	s = (s | s << 2) & 0x00249249;
	return s;
}

template<typename T>
inline uint ZOctree<T>::GetPointElem(const ZVec3& p)const
{
	uint8 elem = Get3DMortonNumber
	(
		(uint8)((p.x - m_RgnMin.x) / m_Unit.x),
		(uint8)((p.y - m_RgnMin.y) / m_Unit.y),
		(uint8)((p.z - m_RgnMin.z) / m_Unit.z)
	);

	return elem;
}

template<typename T>
inline ZOctreeSpace<T>::ZOctreeSpace()
	: m_pLatest(nullptr)
{
}

template<typename T>
inline ZOctreeSpace<T>::~ZOctreeSpace()
{
	if (m_pLatest.GetPtr() != nullptr)
		ResetLink(m_pLatest);
}

template<typename T>
inline void ZOctreeSpace<T>::ResetLink(ZSP<ZObjectForTree<T>>& pOFT)
{
	if (pOFT->m_pNext != nullptr)
		ResetLink(pOFT->m_pNext);
	pOFT = nullptr;
}

template<typename T>
inline bool ZOctreeSpace<T>::Push(ZSP<ZObjectForTree<T>>& pOFT)
{
	if (pOFT == nullptr)
		return false;
	if (pOFT->m_pSpace == this)
		return false;

	if (m_pLatest == nullptr)
		m_pLatest = pOFT;
	else
	{
		// 最新OFTオブジェクト更新
		pOFT->m_pNext = m_pLatest;
		m_pLatest->m_pPrev = pOFT;
		m_pLatest = pOFT;
	}

	pOFT->RegisterSpace(this);
	return true;
}

template<typename T>
inline ZSP<ZObjectForTree<T>>& ZOctreeSpace<T>::GetFirstObj()
{
	return m_pLatest;
}

template<typename T>
inline void ZOctreeSpace<T>::OnRemove(ZObjectForTree<T>*& pRemoveObj)
{
	if (m_pLatest != pRemoveObj)
		return;
	if (m_pLatest != nullptr)
		m_pLatest = m_pLatest->GetNextObj();
}
