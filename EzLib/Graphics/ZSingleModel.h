//===============================================================
//  @file ZSingleModel.h
//   メッシュモデルクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZSingleModel_h
#define	ZSingleModel_h

namespace EzLib
{

	//==========================================================
	//   マテリアルデータ構造体
	// 
	//   １つの材質の情報
	// 
	//  @ingroup Graphics_Model
	//==========================================================
	struct ZMaterial
	{
		//===========================================================
		// マテリアルデータ
		//===========================================================

		ZVec4			Diffuse;			// 拡散色(基本色)
		ZVec4			Specular;			// 反射色
		float			Power = 0;			// 反射の強さ
		ZVec3			Ambient;			// 環境色
		ZVec4			Emissive;			// 自己照明

		ZString		TextureFileName;	// テクスチャ名

		ZString		ExtScript;			// 拡張スクリプト文字列

		ZVec2			UV_Tiling = ZVec2(1, 1);// UVタイリング率
		ZVec2			UV_Offset;			// UVオフセット値

		//------------------------------------------------------------------------
		//   テクスチャセット
		//   複数名のテクスチャを１つの構造体にまとめただけのもの
		//------------------------------------------------------------------------
		ZSP<ZTextureSet>	TexSet;				// テクスチャセット

		//------------------------------------------------------------------------
		//   描画フラグ(XED専用)
		//------------------------------------------------------------------------
		char		DrawFlag = 0;
		//   描画フラグ用定数
		enum
		{
			DF_BOTHFACE = 0x01,	// 両面描画
		};

		//   両面描画が有効かどうか
		bool DF_IsBothFace() { return (DrawFlag& DF_BOTHFACE ? true : false); }
	};



	//==========================================================
	//   単一メッシュモデルクラス
	// 
	//   ZMeshとマテリアルを組み合わせただけのもの
	// 
	//  @ingroup Graphics_Model
	//==========================================================
	class ZSingleModel
	{
	public:
		//=====================================
		// 読み込み・解放
		//=====================================

		//   Xedファイル読み込み
		//  Xedファイルを読み込み、複数メッシュが存在する場合は、先頭の物のみを使用する
		bool LoadMesh(const ZString& filename);

		//------------------------------------------------------------------------
		//   解放
		//------------------------------------------------------------------------
		void Release();

		//------------------------------------------------------------------------
		//   描画に必要なデータをセット
		//  以下の物をデバイスへセットする
		//  ・頂点バッファ
		//  ・インデックスバッファ
		//  ・プリミティブ・トポロジー
		//------------------------------------------------------------------------
		void SetDrawData();

		//=====================================
		// 取得
		//=====================================

		//   メッシュデータを取得
		ZSP<ZMesh>						GetMesh() { return m_pMesh; }

		//   マテリアルデータを取得
		ZAVector<ZMaterial>&			GetMaterials() { return m_Materials; }

		//   メッシュ名取得
		ZString&						GetName() { return m_Name; }

		//=====================================
		// 設定
		//=====================================

		//------------------------------------------------------------------------
		//   ZMeshを外部からセットする
		//  	mesh	… セットするZMeshクラス
		//------------------------------------------------------------------------
		void SetMesh(ZSP<ZMesh> mesh)
		{
			m_pMesh = mesh;
		}

		//------------------------------------------------------------------------
		//   マテリアルリストを外部からセットする
		//  	materials	… セットするマテリアル配列
		//------------------------------------------------------------------------
		void SetMaterials(const ZAVector<ZMaterial>& materials)
		{
			m_Materials.clear();
			m_Materials = materials;
		}

		//------------------------------------------------------------------------
		//   メッシュ名設定
		//  	meshName	… メッシュ名設定
		//------------------------------------------------------------------------
		void SetName(const ZString& meshName) { m_Name = meshName; }


		//
		ZSingleModel();
		//
		~ZSingleModel()
		{
			Release();
		}

	private:
		ZString						m_Name;			// メッシュ名

		ZSP<ZMesh>					m_pMesh;		// メッシュデータ
		ZAVector<ZMaterial>			m_Materials;	// マテリアルデータ
		
	private:
		// コピー禁止用
		ZSingleModel(const ZSingleModel& src) {}
		void operator=(const ZSingleModel& src) {}
	};

}

#endif
