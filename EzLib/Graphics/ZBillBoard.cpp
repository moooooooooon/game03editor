#include "EzLib.h"

using namespace EzLib;

ZWP<ZPolygon>	ZBillBoard::s_wpPolyRectangle;

ZWP<ZRingDynamicVB>	ZBillBoard::s_wpRingVB;


void ZBillBoard::SetVertex(float x, float y, float w, float h)
{
	//[1]      [3]
	// *--------*
	// |＼　　　|
	// |　＼　　|h
	// |　　＼　|
	// |　　　＼|
	// *--------*
	//(x,y) w  [2]
	//[0]

	// 頂点座標
	m_V[0].Pos.x = x;
	m_V[0].Pos.y = y;
	m_V[0].Pos.z = 0;

	m_V[1].Pos.x = x;
	m_V[1].Pos.y = y + h;
	m_V[1].Pos.z = 0;

	m_V[2].Pos.x = x + w;
	m_V[2].Pos.y = y;
	m_V[2].Pos.z = 0;

	m_V[3].Pos.x = x + w;
	m_V[3].Pos.y = y + h;
	m_V[3].Pos.z = 0;

	// 作業用ポリゴンバッファ設定
	if (m_pPoly == nullptr)
	{
		// ポリゴンデータが存在しないなら作成する
		if (s_wpPolyRectangle.IsActive() == false)
		{
			m_pPoly = Make_Shared(ZPolygon,appnew);
			m_pPoly->Create(nullptr, 4, m_V[0].GetVertexTypeData());

			s_wpPolyRectangle = m_pPoly;
		}
		// すでに作成済ならそれを使用
		else
		{
			m_pPoly = s_wpPolyRectangle.Lock();
		}
	}

	if (m_pRingBufPoly == nullptr)
	{
		// ポリゴンデータが存在しないなら作成する
		if (s_wpRingVB.IsActive() == false)
		{
			m_pRingBufPoly = Make_Shared(ZRingDynamicVB,appnew);
			m_pRingBufPoly->Create(m_V[0].GetVertexTypeData(), 3000);

			s_wpRingVB = m_pRingBufPoly;
		}
		// すでに作成済ならそれを使用
		else
		{
			m_pRingBufPoly = s_wpRingVB.Lock();
		}
	}

}

void ZBillBoard::Draw()
{

	ZVec2 uvMin, uvMax;
	m_Anime.GetUV(&uvMin, &uvMax);
	// UV変更
	m_V[0].UV.x = uvMin.x;
	m_V[0].UV.y = uvMax.y;
	m_V[1].UV.x = uvMin.x;
	m_V[1].UV.y = uvMin.y;
	m_V[2].UV.x = uvMax.x;
	m_V[2].UV.y = uvMax.y;
	m_V[3].UV.x = uvMax.x;
	m_V[3].UV.y = uvMin.y;

	// 描画
//	m_pPoly->WriteAndDraw(m_V, 4);

	// 描画
	m_pRingBufPoly->SetDrawData();
	m_pRingBufPoly->WriteAndDraw(m_V, 4);
}

