#include "EzLib.h"
#include "ZGeometryShader.h"

using namespace EzLib;

bool ZGeometryShader::LoadShader(const ZString &csoFileName)
{
	Release();

	m_csoFileName = csoFileName;

	std::ifstream ifs(csoFileName.c_str(), std::ios::binary);
	if (!ifs.fail())
	{
		// ファイルサイズ
		ifs.seekg(0, std::ios::end);
		int len = (int)ifs.tellg();
		ifs.seekg(0, std::ios::beg);

		// Blob(バッファ)作成
		D3DCreateBlob(len, &m_pCompiledShader);
		// 読み込み
		ifs.read((char*)m_pCompiledShader->GetBufferPointer(), len);
	}
	else
	{
		MessageBox(0, "指定したcsoファイルが存在しません", csoFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	//==========================================================================
	// ブロブからジオメトリシェーダー作成
	//==========================================================================
	if (FAILED(ZDx.GetDev()->CreateGeometryShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_GS)))
	{
		MessageBox(0, "ジオメトリシェーダー作成失敗", csoFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	return false;
}

bool ZGeometryShader::LoadShaderFromFile(const ZString& hlslFileName, const ZString& GsFuncName, const ZString& shaderModel, UINT shaderFlag)
{
	Release();

	// 最適化
	#ifdef OPTIMIZESHADER
	shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL3;	// 最大の最適化 リリース用
	#else
	shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_DEBUG;	// 最低の最適化 開発用
	#endif


	ID3DBlob *pErrors = nullptr;

	m_hlslFileName = hlslFileName;

	// csoファイル名生成( [hlslFileName]_[VsFuncName].cso )
	char szDir[_MAX_PATH];
	char szFname[_MAX_FNAME];
	char szExt[_MAX_EXT];
	_splitpath_s(hlslFileName.c_str(), nullptr,0, szDir,_MAX_PATH, szFname,_MAX_FNAME, szExt,_MAX_EXT);

	m_csoFileName = szDir;
	m_csoFileName += szFname;
	m_csoFileName += "_";
	m_csoFileName += GsFuncName;
	m_csoFileName += ".cso";


	//==========================================================================
	// HLSLからピクセルシェーダをコンパイル
	//==========================================================================
	if (m_pCompiledShader == nullptr)
	{
		if (FAILED(D3DCompileFromFile(ConvertStringToWString(hlslFileName).c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, GsFuncName.c_str(), shaderModel.c_str(), shaderFlag, 0, &m_pCompiledShader, &pErrors)))
		{
			if (pErrors)
			{
				MessageBox(0, (char*)pErrors->GetBufferPointer(), "HLSLコンパイル失敗", MB_OK);
			}
			else
			{
				MessageBox(0, "HLSLファイルが存在しない", hlslFileName.c_str(), MB_OK);
			}
			return false;
		}
		Safe_Release(pErrors);
	}

	//==========================================================================
	// ブロブからジオメトリシェーダー作成
	//==========================================================================
	if (FAILED(ZDx.GetDev()->CreateGeometryShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_GS)))
	{
		MessageBox(0, "ジオメトリシェーダー作成失敗", hlslFileName.c_str(), MB_OK);
		Release();
		return false;
	}

	return true;
}

bool ZGeometryShader::SaveToFile()
{
	if (m_pCompiledShader == nullptr)return false;

	//==========================================================================
	// コンパイル済みのシェーダを保存する
	//==========================================================================
	std::ofstream ofs(m_csoFileName.c_str(), std::ios::binary);
	ofs.write((char*)m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize());

	return true;
}

void ZGeometryShader::Release()
{
	Safe_Release(m_pCompiledShader);
	Safe_Release(m_GS);
}

void ZGeometryShader::SetShader()
{
	// ジオメトリシェーダ設定
	ZDx.GetDevContext()->GSSetShader(m_GS, 0, 0);
}

void ZGeometryShader::DisableShader()
{
	// ジオメトリシェーダ解除
	ZDx.GetDevContext()->GSSetShader(nullptr, 0, 0);
}

EzLib::ZGeometryShader::ZGeometryShader() : m_GS(0), m_pCompiledShader(0)
{
}