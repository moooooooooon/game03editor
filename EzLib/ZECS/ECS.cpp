#include "ZECS.h"

namespace EzLib
{
namespace ZECS
{
	EntityComponentSystem::EntityComponentSystem()
		: m_CompRefrection(&m_CompMemories),
		m_UseMultiThread(false)
	{
		m_NumThread = std::thread::hardware_concurrency() - 1;
		m_ThreadPool = Make_Unique(ZThreadPool,sysnew,m_NumThread);
		m_CollectSystemParamFutures.resize(m_ThreadPool->GetNumThreads());
	}

	EntityComponentSystem::~EntityComponentSystem()
	{
		m_CompRefrection.Release();
		RemoveAllEntity();


		for (auto& d : m_CompMemories) {
			d.second.clear();
		}
		m_CompMemories.clear();

		__ECSComponentBase::Release();
	}

	void EntityComponentSystem::RemoveEntity(ECSEntity& entity)
	{
		std::lock_guard<std::mutex> lg(m_EntityMtx);
		_RemoveEntityComps(entity.m_EntityHandle);
		entity.m_EntityHandle = NULL_ENTITY_HANDLE;
	}

	void EntityComponentSystem::RemoveAllEntity()
	{
		std::lock_guard<std::mutex> lg(m_EntityMtx);
		for(size_t i = m_Entities.size()-1;m_Entities.size() > 0; i = m_Entities.size() - 1)
			_RemoveEntityComps(i);
		
		m_Entities.resize(0);
	}

	void EntityComponentSystem::UpdateSystems(ECSSystemList& systems,const float delta, bool notUseLateUpdate)
	{
		std::lock_guard<std::mutex> lg(m_ECSMtx);
		
		// systemごとのコンポーネントパッケージの収集(マルチスレッド)
		{
			ECSThreadData data;
			data.size = systems.Size();

			auto execThread = [this,&systems,&data](size_t index)
			{
				if (index >= data.size)
					return UpdateComponentPackageList();

				auto& system = systems[index];
				return CollectComponentPackage(system);
			};

			auto& futures = m_CollectSystemParamFutures;
	
			if(futures.size() < data.size)
				futures.resize(data.size);

			for (size_t i = 0; i < data.size; i++)
			{
				if (systems[i]->IsActive() == false)
					continue;

				futures[i] = m_ThreadPool->AddTask([this,execThread,i]()
				{
					return execThread(i);
				});
			}

			for (size_t i = 0;i< data.size; i++)
			{
				if (systems[i]->IsActive() == false)
					continue;

				m_SystemsUpdateParams[systems[i]] = std::move(futures[i].get());
			}
		}
		
		for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)
		{
			if(updateCnt == 1 && notUseLateUpdate == true)
				return;

			for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
			{
				if (systems[updateIndex]->IsActive() == false)
					continue;

				auto& compTypes = systems[updateIndex]->GetComponentTypes();
				
				// 要求されたコンポーネントが0個でも更新
				if(compTypes.size() == 0)
				{
					if (updateCnt == 0)
						systems[updateIndex]->UpdateComponents(delta, nullptr);
					else
						systems[updateIndex]->LateUpdateComponents(delta, nullptr);
					continue;
				}

				bool isLateUpdate = (updateCnt == 1);
				auto& pacageList = m_SystemsUpdateParams[systems[updateIndex]];
				_UpdateSystem(systems[updateIndex], delta, pacageList, isLateUpdate);
			
			}	//	for (uint32 updateIndex = 0; updateIndex < systems.Size(); updateIndex++)
		
		} // for (uint32 updateCnt = 0; updateCnt < 2; updateCnt++)

		for (auto& it : m_SystemsUpdateParams)
			it.second.clear();

	}

	void EntityComponentSystem::EnableUseMultiThread()
	{
		std::lock_guard<std::mutex> lg(m_ECSMtx);
		m_UseMultiThread = true;
	}

	void EntityComponentSystem::DisableUseMultiThread()
	{
		std::lock_guard<std::mutex> lg(m_ECSMtx);
		m_UseMultiThread = false;
	}

	void EntityComponentSystem::GetComponentNames(ZVector<ZString>& out)
	{
		out.clear();
		for (auto& cl : m_CompRefrection.GetMap()) 
		{
			out.push_back(cl.first);
		}
	}

	void EntityComponentSystem::_RemoveEntityComps(EntityHandle entityHandle)
	{
		if (entityHandle == NULL_ENTITY_HANDLE)
			return;
		
		auto& entityComps = HandleToEntityComps(entityHandle);
		
		// リスナーに通知
		for (auto& listener : m_ECSListeners)
		{
			if (listener->NotifyOnAllEntityOperations())
			{
				listener->OnRemoveEntity(entityHandle);
				continue;
			}

			ComponentBitSet tmpBit = listener->GetComponentBitSet();
			tmpBit &= HandleToEntitySptr(entityHandle)->m_CompBitSet;
			if (tmpBit == 0)
				continue;

			listener->OnRemoveEntity(entityHandle);
		}

		for(auto comp : entityComps)
			DeleteComponent(comp.first, comp.second);

		uint32 destIndex = entityHandle;
		uint32 srcIndex = m_Entities.size() - 1;
		
		m_Entities[destIndex] = m_Entities[srcIndex];
		m_Entities.pop_back();
		
		if (destIndex >= m_Entities.size())
			return;
		m_Entities[destIndex].first->m_EntityHandle = destIndex;
	}

	void EntityComponentSystem::DeleteComponent(uint32 componentID,uint32 compIndex)
	{
		ComponentMemory& compArray = m_CompMemories[componentID];
		ECSComponentFreeFunction freeFunc = __ECSComponentBase::GetTypeFreeFunction(componentID);
		size_t typeSize = __ECSComponentBase::GetTypeSize(componentID);
		uint32 srcIndex = compArray.size() - typeSize;

		auto* destComponent = (__ECSComponentBase*)(&compArray[compIndex]);
		auto* srcComponent = (__ECSComponentBase*)(&compArray[srcIndex]);
		freeFunc(destComponent);
		
 		if(compIndex == srcIndex)
		{
			compArray.resize(srcIndex);
			return;
		}
		memcpy(destComponent, srcComponent, typeSize);

		// 一応登録されているかチェック
		srcComponent->m_MemoryIndex = compIndex;

		auto& srcComponents = HandleToEntityComps(srcComponent->m_Entity->m_EntityHandle);
		
		auto& it = srcComponents.find(componentID);

		if (it != srcComponents.end() && (*it).second == srcIndex)
			srcComponents[componentID] = compIndex;

		compArray.resize(srcIndex);
	}

	EntityComponentSystem::UpdateComponentPackageList EntityComponentSystem::CollectComponentPackage(ZSP<ECSSystemBase> system)
	{
		UpdateComponentPackageList packageList;
		auto compTypes = system->GetComponentTypes();
		
		if (compTypes.size() == 0)
			return std::move(packageList);
		// 単一のコンポーネントなら
		else if (compTypes.size() == 1)
		{
			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[0]);
			ComponentMemory& compMemArray = m_CompMemories[compTypes[0]];
			size_t numComps = compMemArray.size() / typeSize;
			packageList.resize(numComps);
			for (uint32 i = 0; i < compMemArray.size(); i += typeSize)
			{
				size_t entityIndex = i / typeSize;
				__ECSComponentBase* comp = (__ECSComponentBase*)&compMemArray[i];
				packageList[entityIndex].push_back(comp);
			}
			return std::move(packageList);
		}
		// 複数のコンポーネントなら
		else if(compTypes.size() > 1)
		{
			auto& bitSet = system->GetBitSet();

			// systemのコンポーネントフラグ取得
			const ZSVector<uint32>& compFlags = system->GetComponentFlags();

			ComponentArray compMemArray(compTypes.size());
			for (uint32 i = 0; i < compTypes.size(); i++)
				compMemArray[i] = &m_CompMemories[compTypes[i]];

			// 要求された中で一番インスタンスの少ないコンポーネントタイプのインデックスを取得
			uint32 minSizeIndex = FindLeastCommonComponent(compTypes, compFlags);

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[minSizeIndex]);
			ComponentMemory& compArray = *compMemArray[minSizeIndex]; // 一番少ないコンポーネントの配列
			size_t numConpnents = compArray.size() / typeSize;
			packageList.reserve(numConpnents);
			
			for (uint32 i = 0; i < compArray.size(); i += typeSize)
			{
				// コンポーネントをもつエンティティがシステムの要求するコンポーネントを全て持っているか
				auto comp = (__ECSComponentBase*)(&(compArray[i]));
				ComponentBitSet tmpBit = bitSet;
				tmpBit &= comp->m_Entity->m_CompBitSet;
				if (tmpBit != bitSet)
					continue;
				
				packageList.emplace_back();
				UpdateComponentPackage& package = packageList.back();
				package.resize(compTypes.size());
				package[minSizeIndex] = (__ECSComponentBase*)(&(compArray[i]));

				// コンポーネントからEntity取得
				auto& entityComponents = HandleToEntityComps(comp->m_Entity->m_EntityHandle);

				// 取得したEntityから残りのコンポーネント取得
				for (uint32 j = 0; j < compTypes.size(); j++)
				{
					if (j == minSizeIndex)
						continue;
					package[j] = _GetComponent(entityComponents, *compMemArray[j], compTypes[j]);
				}
			} // for (uint32 i = 0; i < compArray.size(); i += typeSize)

		} // else if(compTypes.size() > 1)

		return std::move(packageList);
	}

	void EntityComponentSystem::_UpdateSystem(ZSP<ECSSystemBase> updateSystem, float delta, UpdateComponentPackageList& pacageList, bool isLateUpdate)
	{
		// ECS自体、またはシステムのマルチスレッドでの更新が有効でなければシングルスレッドで更新
		if (m_UseMultiThread == false || updateSystem->UseMultiThread() == false)
		{
			for (auto& compPackage : pacageList)
			{
				if (isLateUpdate == false)
					updateSystem->UpdateComponents(delta, &compPackage[0]);
				else
					updateSystem->LateUpdateComponents(delta, &compPackage[0]);
			}

			return;
		}

		// ECS自体とシステムのマルチスレッドでの更新が有効であればマルチスレッドで更新(テスト段階)
		ECSThreadData data;
		data.size = pacageList.size();
		data.nowIndex = 0;
		
		auto execThread = [this, &data, &pacageList, isLateUpdate, updateSystem, delta]()
		{
			size_t updateElem = data.size / m_ThreadPool->GetNumThreads();
			if (updateElem <= 0)
				updateElem = 1;

			while (true)
			{
				size_t index = data.nowIndex.fetch_add(updateElem);
				if (index > data.size)
					break;
				for (size_t i = 0; i < updateElem; i++)
				{
					if (index + i >= data.size)
						break;

					if (isLateUpdate == false)
						updateSystem->UpdateComponents(delta, &pacageList[index + i][0]);
					else
						updateSystem->LateUpdateComponents(delta, &pacageList[index + i][0]);
				}
			}
		};

		// 並列処理
		{
			auto numThreads = m_ThreadPool->GetNumThreads();
			
			for (size_t i = 0; i < numThreads; i++)
				m_ThreadPool->AddTask(execThread);
			m_ThreadPool->WaitForAllTasksFinish();
		}

	}

	uint32 EntityComponentSystem::FindLeastCommonComponent(const ZSVector<uint32>& compTypes, const ZSVector<uint32>& compFlags)
	{
		// 指定されたコンポーネント一覧の中で一番保持している数が少ないコンポーネントのIDを取得
		
		uint32 minSize = (uint32)-1; // オーバーフロー -> uint32の最大値
		uint32 minIndex = (uint32)-1; // オーバーフロー -> uint32の最大値

		for(uint32 i = 0;i<compTypes.size();i++)
		{
			if ((compFlags[i]& ECSSystemBase::FLAG_OPTIONAL) != 0)
				continue;

			size_t typeSize = __ECSComponentBase::GetTypeSize(compTypes[i]);

			uint32 size = m_CompMemories[compTypes[i]].size()/ typeSize;
			if (size > minSize)
				continue;
			minSize = size;
			minIndex = i;
		}

		return minIndex;
	}

	void EntityComponentSystem::GetCompList(EntityHandle h, std::vector<__ECSComponentBase*>& list) {
		EntityComps& compArray = HandleToEntityComps(h);

		for (auto& it : compArray) {
			auto& Mem = m_CompMemories[it.first];
			list.push_back(reinterpret_cast<__ECSComponentBase*>(&Mem[it.second]));
		}
	}
}
}