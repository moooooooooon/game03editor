#include "ECSComponent.h"

namespace EzLib
{
namespace ZECS
{

	std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>* __ECSComponentBase::ComponentTypes = nullptr;
	
	uint32 __ECSComponentBase::RegisterComponentType(ECSComponentCreateFunction crateFunc, ECSComponentFreeFunction freeFunc,size_t size)
	{
		if (ComponentTypes == nullptr)
			ComponentTypes = new std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>;
		uint32 componentID = ComponentTypes->size();
		ComponentTypes->push_back(std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>(crateFunc, freeFunc,size));

		return componentID;
	}



}
}