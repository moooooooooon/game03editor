#include "ECSComponent.h"
inline ECSComponentCreateFunction __ECSComponentBase::GetTypeCreateFunction(uint32 id)
{
	return std::get<0>((*ComponentTypes)[id]);
}

inline  ECSComponentFreeFunction __ECSComponentBase::GetTypeFreeFunction(uint32 id)
{
	return std::get<1>((*ComponentTypes)[id]);
}

inline size_t __ECSComponentBase::GetTypeSize(uint32 id)
{
	return std::get<2>((*ComponentTypes)[id]);
}

// 有効なコンポーネントか
inline bool __ECSComponentBase::IsTypeValid(uint32 id)
{
	return id < ComponentTypes->size();
}

inline const UUID& EzLib::ZECS::__ECSComponentBase::GetUUID()const
{
	return m_UUID;
}

inline void EzLib::ZECS::__ECSComponentBase::SetUUID(const UUID & uuid)
{
	m_UUID = uuid;
}
