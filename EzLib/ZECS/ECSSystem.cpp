#include "ECSSystem.h"

namespace EzLib
{
namespace ZECS
{
	void ECSSystemBase::DebugImGuiRender()
	{
		ImGui::PushID(m_DebugSystemName.c_str());
		ImGui::SameLine(0, 20);
		ImGui::Checkbox("System Enable", &m_IsEnable);
		ImGui::PopID();
	}

	bool ECSSystemList::RemoveSystem(ZSP<ECSSystemBase> system)
	{
		for (uint32 i = 0; i < m_Systems.size(); i++)
		{
			if (system == m_Systems[i])
			{
				m_Systems.erase(m_Systems.begin() + i);
				return true;
			}
		}
		return false;

	}

}
}
