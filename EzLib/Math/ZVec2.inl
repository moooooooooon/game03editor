namespace EzLib
{

	//------------------------------------------------------------------------------
	// ��r���Z�q
	//------------------------------------------------------------------------------

	inline bool ZVec2::operator == (const ZVec2& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR v2 = XMLoadFloat2(&V);
		return XMVector2Equal(v1, v2);
	}

	inline bool ZVec2::operator != (const ZVec2& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR v2 = XMLoadFloat2(&V);
		return XMVector2NotEqual(v1, v2);
	}

	//------------------------------------------------------------------------------
	// ������Z�q
	//------------------------------------------------------------------------------

	inline ZVec2& ZVec2::operator+= (const ZVec2& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR v2 = XMLoadFloat2(&V);
		XMVECTOR X = XMVectorAdd(v1, v2);
		XMStoreFloat2(this, X);
		return *this;
	}

	inline ZVec2& ZVec2::operator-= (const ZVec2& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR v2 = XMLoadFloat2(&V);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		XMStoreFloat2(this, X);
		return *this;
	}

	inline ZVec2& ZVec2::operator*= (const ZVec2& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR v2 = XMLoadFloat2(&V);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		XMStoreFloat2(this, X);
		return *this;
	}

	inline ZVec2& ZVec2::operator*= (float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR X = XMVectorScale(v1, S);
		XMStoreFloat2(this, X);
		return *this;
	}

	inline ZVec2& ZVec2::operator/= (float S)
	{
		using namespace DirectX;
		assert(S != 0.0f);
		XMVECTOR v1 = XMLoadFloat2(this);
		XMVECTOR X = XMVectorScale(v1, 1.f / S);
		XMStoreFloat2(this, X);
		return *this;
	}

	inline float ZVec2::Dot(const ZVec2& v1, const ZVec2& v2)
	{
		using namespace DirectX;
		XMVECTOR V1 = XMLoadFloat2(&v1);
		XMVECTOR V2 = XMLoadFloat2(&v2);
		XMVECTOR X = XMVector2Dot(V1, V2);
		return XMVectorGetX(X);
	}

	inline float ZVec2::DotClamp(const ZVec2& v1, const ZVec2& v2)
	{
		float retdot = Dot(v1, v2);
		if (retdot < -1.0f)
		{
			retdot = -1.0f;
		}
		if (retdot > 1.0f)
		{
			retdot = 1.0f;
		}
		return retdot;
	}

	inline float ZVec2::Length(const ZVec2& vSrc)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&vSrc);
		XMVECTOR X = XMVector2Length(v1);
		return XMVectorGetX(X);
	}

	inline void ZVec2::Normalize(ZVec2& vOut, const ZVec2& vSrc)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&vSrc);
		XMVECTOR X = XMVector2Normalize(v1);
		XMStoreFloat2(&vOut, X);
	}

	inline void ZVec2::Lerp(ZVec2& vOut, const ZVec2& v1, const ZVec2& v2, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat2(&v1);
		XMVECTOR x2 = XMLoadFloat2(&v2);
		XMVECTOR X = XMVectorLerp(x1, x2, f);
		XMStoreFloat2(&vOut, X);
	}

	inline void Hermite(ZVec2& vOut, const ZVec2& v1, const ZVec2& t1, const ZVec2& v2, const ZVec2& t2, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat2(&v1);
		XMVECTOR x2 = XMLoadFloat2(&t1);
		XMVECTOR x3 = XMLoadFloat2(&v2);
		XMVECTOR x4 = XMLoadFloat2(&t2);
		XMVECTOR X = XMVectorHermite(x1, x2, x3, x4, f);
		XMStoreFloat2(&vOut, X);
	}

	inline void ZVec2::CatmullRom(ZVec2& vOut, const ZVec2& v0, const ZVec2& v1, const ZVec2& v2, const ZVec2& v3, float f)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat2(&v0);
		XMVECTOR x2 = XMLoadFloat2(&v1);
		XMVECTOR x3 = XMLoadFloat2(&v2);
		XMVECTOR x4 = XMLoadFloat2(&v3);
		XMVECTOR X = XMVectorCatmullRom(x1, x2, x3, x4, f);
		XMStoreFloat2(&vOut, X);
	}

	//------------------------------------------------------------------------------
	// Binary operators
	//------------------------------------------------------------------------------

	inline ZVec2 operator+ (const ZVec2& V1, const ZVec2& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V1);
		XMVECTOR v2 = XMLoadFloat2(&V2);
		XMVECTOR X = XMVectorAdd(v1, v2);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline ZVec2 operator- (const ZVec2& V1, const ZVec2& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V1);
		XMVECTOR v2 = XMLoadFloat2(&V2);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline ZVec2 operator* (const ZVec2& V1, const ZVec2& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V1);
		XMVECTOR v2 = XMLoadFloat2(&V2);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline ZVec2 operator* (const ZVec2& V, float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline ZVec2 operator/ (const ZVec2& V1, const ZVec2& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V1);
		XMVECTOR v2 = XMLoadFloat2(&V2);
		XMVECTOR X = XMVectorDivide(v1, v2);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline ZVec2 operator* (float S, const ZVec2& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec2 R;
		XMStoreFloat2(&R, X);
		return R;
	}

	inline void ZVec2::Transform(ZVec2 &vOut, const ZVec2 &vSrc, const ZMatrix &mat)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&vSrc);
		XMMATRIX M = XMLoadFloat4x4(&mat);
		XMVECTOR X = XMVector2TransformCoord(v1, M);
		XMStoreFloat2(&vOut, X);
	}

	inline void ZVec2::TransformNormal(ZVec2 &vOut, const ZVec2 &vSrc, const ZMatrix &mat)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat2(&vSrc);
		XMMATRIX M = XMLoadFloat4x4(&mat);
		XMVECTOR X = XMVector2TransformNormal(v1, M);
		XMStoreFloat2(&vOut, X);
	}


}