#ifndef __ZMATH_H__
#define __ZMATH_H__

#include "Math/ZVec2.h"
#include "Math/ZVec3.h"
#include "Math/ZVec4.h"
#include "Math/ZQuat.h"
#include "Math/ZMatrix.h"
#include "Math/ZOperationalMathClasses.h"

#include "Math/ZMatrix.inl"
#include "Math/ZOperationalMathClasses.inl"
#include "Math/ZVec2.inl"
#include "Math/ZVec3.inl"
#include "Math/ZVec4.inl"
#include "Math/ZQuat.inl"

#endif