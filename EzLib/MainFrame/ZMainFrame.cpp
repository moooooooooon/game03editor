#include "MainFrame/ZMainFrame.h"
#include <iostream>
#include <thread>

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace EzLib
{
	LRESULT MainFrame_WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		auto* window = EzLib::ZWindow::GetWindowClass(hWnd);
		if (window == nullptr)
			return 0;

		ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam);

		// メッセージ処理
		switch (msg)
		{
			case WM_ACTIVATE:
			{
				bool isActive;

				// 非アクティブ
				if (LOWORD(wParam) == WA_INACTIVE)
					isActive = false;
				else
					isActive = true;

				window->SetWindowActive(isActive);
			}
			break;

			case WM_MOUSEWHEEL:
				INPUT.SetMouseWheelValue((short)HIWORD(wParam));
			break;

			case WM_DESTROY:
				PostQuitMessage(0);
			return 0;
		}

		return 0;
	}

	ZMainFrame* ZMainFrame::m_sInstance = nullptr;

	#ifdef SHOW_DEBUG_WINDOW

	void DebugCommandProc(ZString cmdText)
	{

	}

	#endif

	ZMainFrame::ZMainFrame(const char * wndTitle, const ZWindowProperties& properties)
		: m_FrameRate(600), m_IsExitGameLoop(false), m_WndTitle(wndTitle), m_WndProperties(properties)
	{
		m_OneFrameParMilli = (1000.0ms / m_FrameRate);
		m_DeltaTime = 1.0f / m_FrameRate;
		m_Rand.InitSeed(timeGetTime());
		m_sInstance = this;

	}

	ZMainFrame::~ZMainFrame()
	{
		if (m_IsFullScreen)
			ChangeDisplaySettings(NULL, 0);
		DeleteInstance();
	}

	void ZMainFrame::Start()
	{
		if (Init() == false)
			return;

		Run();
	}

	bool ZMainFrame::Init()
	{
		m_Window = std::make_unique<ZWindow>(m_WndTitle.c_str(), m_WndProperties);
		if (m_Window->IsInitialized() == false)
			return false;

		// DirectX初期化
		ZDirectXGraphics::CreateInstance();
		ZString errorMsg;
		if (ZDx.Init(m_Window->GetWindowHandle(), m_WndProperties.Width, m_WndProperties.Height, &m_ResStg, ZDirectXGraphics::MSAA::MSAA_NONE, &errorMsg) == false)
		{
			errorMsg = "DirectX初期化失敗 : " + errorMsg;
			MessageBox(m_Window->GetWindowHandle(), errorMsg.c_str(), "Error", MB_OK);
			return false;
		}
		ZDx.SetVSync(m_WndProperties.UseVSync);

		// フォント管理設定
		ZFontSpriteManager::CreateInstance();
		ZFontMgr.Init(m_Window->GetWindowHandle());
		// フォント追加
		ZFontMgr.AddFont(0, "ＭＳ ゴシック", 12);
		ZFontMgr.AddFont(1, "ＭＳ ゴシック", 24);
		ZFontMgr.AddFont(2, "ＭＳ ゴシック", 36);

		// シェーダー初期化
		ShMgr.Init();

		// サウンド初期化
		ZSndMgr.Init();

		//VRデバイス設定
	//	CVR::CreateInstance();
	//	cvr.Init();
		
		if (m_WndProperties.UseFullScreen)
			ZDx.SetFullScreen(true);

	#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Init(m_Window->GetWindowHandle(), 4, DebugCommandProc);
	#endif

		// エディタ初期化
		EditSystem.Init();

		m_ColEng.Init();

		// シーンマネージャー初期化
		m_SceneMgr.Init();

		// 物理ワールド初期化
		m_PhysicsWorld.Init();
		
		return true;
	}

	void ZMainFrame::Run()
	{
		// スリープ時間計測用
		std::chrono::duration<double> sleepDurationTime;
	
		ZTimer timer(m_DulationTime);
		ZTimer stimer(sleepDurationTime); // スリープ時間計測

		// ケームループ
		while (m_IsExitGameLoop == false)
		{
			// ウィンドウメッセージ処理,入力情報取得
			m_Window->Update();
			if (m_Window->IsClosed())break; // ウィンドウが閉じられていたら即終了

			// ゲーム処理
			timer.Start();
			// サウンド処理
			ZSndMgr.Update();

			// デバッグウィンドウ表示切り替え
			if (INPUT.KeyStay(VK_CONTROL) && INPUT.KeyEnter('D'))
				DEBUG_WINDOW.ToggleWindow();

			// Update~
			{
				m_SceneMgr.Update();
#ifdef SHOW_DEBUG_WINDOW
				DEBUG_WINDOW.Update();
#endif
				INPUT.SetMouseWheelValue(0);
				
				// メモリ詳細出力
				constexpr int beginMemoryScrollIndex = 10;
				for (size_t i = 0; i < MEMORY_AREA_TYPE::NUM_TYPE; i++)
				{
					const std::string& memoryName = ZAllocator::GetMemoryAreaName((MEMORY_AREA_TYPE)i);
					const ZMemoryAllocator::AllocationInfo& memoryInfo = ZAllocator::GetAllocationInfo((MEMORY_AREA_TYPE)i);
					std::string unit[4] = { "Byte" };
					size_t infoValues[4] = {
												ZAllocator::GetMemorySize((MEMORY_AREA_TYPE)i),
												memoryInfo.ActiveMemory,
												memoryInfo.AllAllocSize,
												memoryInfo.AllFreeSize
											};

					for(size_t i = 0;i<4;i++)
					{
						auto& value = infoValues[i];
						if(value > 1_KB)
						{
							if (value > 1_MB)
							{
								value = Byte2MB(value);
								unit[i] = "MB";
							}
							else
							{
								value = Byte2KB(value);
								unit[i] = "KB";
							}
						}
					}

					DW_STATIC(beginMemoryScrollIndex + i * 6 + 0, "----------------- %s -----------------", memoryName.c_str());
					DW_STATIC(beginMemoryScrollIndex + i * 6 + 1, "MemorySize   : %d %s",infoValues[0], unit[0].c_str());
					DW_STATIC(beginMemoryScrollIndex + i * 6 + 2, "ActiveMemory : %d %s",infoValues[1], unit[1].c_str());
					DW_STATIC(beginMemoryScrollIndex + i * 6 + 3, "AllAllocSize : %d %s",infoValues[2], unit[2].c_str());
					DW_STATIC(beginMemoryScrollIndex + i * 6 + 4, "AllFreeSize  : %d %s",infoValues[3], unit[3].c_str());
				}
			
			}

			// Draw~
			{
				static int m_FrameCnt = 0;
				m_FrameCnt++;
				if (m_FrameCnt % 2 == 0)
				{
					// 描画前準備(バックバッファ& Zバッファクリア)
					// バックバッファをクリアする。
					ZDx.Begin(ZVec4(0.3f, 0.3f, 0.8f, 1.0f), true, true);
					
					m_SceneMgr.Draw();

#ifdef SHOW_DEBUG_WINDOW
					m_SceneMgr.ImGuiUpdate();
					DEBUG_WINDOW.Draw();
#endif
					ZDx.End();
				}
			}

			timer.Stop();

			#pragma region FPS Control
			
			FpsControll(stimer, sleepDurationTime);

			// 経過時間保持
			m_DeltaTime = (float)(m_DulationTime.count());

			#pragma endregion

			// FPS描画
			ShowFps();
		}

		Release();
	}

	void ZMainFrame::Release()
	{
		// フォント解放
		ZFontSpriteManager::DeleteInstance();
		ZFontSpriteManager::sRemoveAllFontResource(); // 読み込んだttfファイル削除

		// シーン解放
		m_SceneMgr.Release();
		// シェーダー解放
		ShMgr.Release();
		// リソース解放
		m_ResStg.Release();
		// サウンド解放
		ZSndMgr.Release();
		
		m_PhysicsWorld.Release();

	//	m_CollEngine.Release();

		// デバッグ用GUI解放処理
		#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Release();
		#endif


		//	エディター解放
		EditSystem.Release();

		//VRデバイス解放
	//	cvr.Release();
	//	CVR::DeleteInstance();

		// インスタンス削除
		ZDirectXGraphics::DeleteInstance();
	}

	void ZMainFrame::FpsControll(ZTimer& stimer, chrono::duration<double>& sleepDurationTime)
	{
		if (m_DulationTime >= m_OneFrameParMilli)
			return;

		// 処理時間が指定フレームレートの1フレームあたりの時間より小さければ
		
		// ミリ秒単位のスリープ
		stimer.Start();
		timeBeginPeriod(1);
		DWORD t = (DWORD)((m_OneFrameParMilli - m_DulationTime).count() * 1000);
		Sleep(t);
		timeEndPeriod(1);
		stimer.Stop();
		m_DulationTime += sleepDurationTime;

		// ↑のマイクロ秒単位の端数の時間分Sleep(0)をはさんでの待機ループ
		while (m_DulationTime < m_OneFrameParMilli)
		{
			stimer.Start();
			Sleep(0);
			stimer.Stop();
			m_DulationTime += sleepDurationTime;
		}
		
	}

	bool ZMainFrame::ShowFps()
	{
		static chrono::duration<double> fpsTimeCounter = 0ms;
		static float frameCnt = 0;

		frameCnt += 1;
		fpsTimeCounter += m_DulationTime;

		if (fpsTimeCounter >= 1000ms)
		{
			#ifdef SHOW_DEBUG_WINDOW
			DW_STATIC(2, "%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			DW_STATIC(3, "%3.0f Frame/s\n", frameCnt);

			#else
			DebugLog("%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			#endif

			fpsTimeCounter = 0ms;
			frameCnt = 0;
			return true;
		}

		return false;

	}


}